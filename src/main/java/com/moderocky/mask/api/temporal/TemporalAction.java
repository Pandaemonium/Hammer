package com.moderocky.mask.api.temporal;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dev.moderocky.mirror.Mirror;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class TemporalAction implements Runnable {

    public final Instant time;
    protected boolean complete = false;

    public TemporalAction(Instant time) {
        this.time = time;
    }

    protected TemporalAction(JsonObject object) {
        this(Instant.ofEpochSecond(object.get("completion_time").getAsLong()));
        complete = object.get("is_complete").getAsBoolean();
    }

    private TemporalAction(String string) {
        this(JsonParser.parseString(string).getAsJsonObject());
    }

    public static <ActionT extends TemporalAction> ActionT deserialise(String string) {
        JsonObject object = JsonParser.parseString(string).getAsJsonObject();
        String path = object.get("class_loader").getAsString();
        Mirror<Class<ActionT>> mirror = Mirror.mirror(path.replaceFirst("class ", "").trim());
        return mirror.<ActionT>constructor(JsonObject.class).newInstance(object);
    }

    public static <ActionT extends TemporalAction> ActionT deserialise(JsonElement element) {
        JsonObject object = element.getAsJsonObject();
        String path = object.get("class_loader").getAsString();
        Mirror<Class<ActionT>> mirror = Mirror.mirror(path.replaceFirst("class ", "").trim());
        return mirror.<ActionT>constructor(JsonObject.class).newInstance(object);
    }

    public boolean isPassed() {
        return !Instant.now().isBefore(getCompletionTime());
    }

    public boolean isPending() {
        return !complete && Instant.now().isBefore(getCompletionTime());
    }

    public boolean isComplete() {
        return complete;
    }

    public void schedule(ScheduledExecutorService service) {
        if (complete) return;
        if (isPassed()) {
            run();
            return;
        }
        service.schedule(this, getDelay(), TimeUnit.SECONDS);
    }

    public long getDelay() {
        return Instant.now().until(getCompletionTime(), ChronoUnit.SECONDS);
    }

    public Instant getCompletionTime() {
        return time;
    }

    public abstract JsonObject getAsJson();

    public final JsonObject serialise() {
        JsonObject object = getAsJson() != null ? getAsJson() : new JsonObject();
        object.addProperty("class_loader", this.getClass().getName());
        object.addProperty("is_complete", complete);
        object.addProperty("completion_time", this.getCompletionTime().getEpochSecond());
        return object;
    }

}
