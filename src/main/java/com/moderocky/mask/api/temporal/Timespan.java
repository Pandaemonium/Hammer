package com.moderocky.mask.api.temporal;

import com.moderocky.mask.api.MagicStringList;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.List;

public class Timespan implements TemporalAmount {

    private final Duration duration;

    public Timespan(Duration duration) {
        this.duration = duration;
    }

    public Timespan(long seconds) {
        this(Duration.of(seconds, ChronoUnit.SECONDS));
    }

    public Timespan(long amount, TemporalUnit unit) {
        this(Duration.of(amount, unit));
    }

    @Override
    public String toString() {
        return toString(false);
    }

    public String toString(boolean verbose) {
        long seconds, minutes = 0, hours = 0, days = 0, weeks = 0, years = 0;
        seconds = duration.getSeconds();
        while (seconds >= 60) {
            seconds -= 60;
            minutes++;
        }
        while (minutes >= 60) {
            minutes -= 60;
            hours++;
        }
        while (hours >= 24) {
            hours -= 24;
            days++;
        }
        while (days >= 365) {
            days -= 365;
            years++;
        }
        while (days >= 7) {
            days -= 7;
            weeks++;
        }
        MagicStringList list = new MagicStringList();
        if (years > 0) list.add(years + (verbose ? " years" : "y"));
        if (weeks > 0) list.add(weeks + (verbose ? " weeks" : "w"));
        if (days > 0) list.add(days + (verbose ? " days" : "d"));
        if (hours > 0) list.add(hours + (verbose ? " hours" : "h"));
        if (minutes > 0) list.add(minutes + (verbose ? " minutes" : "m"));
        if (seconds > 0) list.add(seconds + (verbose ? " seconds" : "s"));
        return list.join(verbose ? ", " : "");
    }

    @Override
    public long get(TemporalUnit unit) {
        return duration.get(unit);
    }

    @Override
    public List<TemporalUnit> getUnits() {
        return duration.getUnits();
    }

    @Override
    public Temporal addTo(Temporal temporal) {
        return duration.addTo(temporal);
    }

    @Override
    public Temporal subtractFrom(Temporal temporal) {
        return duration.subtractFrom(temporal);
    }
}
