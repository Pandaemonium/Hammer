package com.moderocky.mask.api;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * A few frequently-used file methods.
 */
public class FileManager {

    public static void putIfAbsent(@NotNull File file) {
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                //
            }
        }
    }

    public static void clear(@NotNull File file) {
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (Exception ignore) {
        }
    }

    public static InputStreamReader getReader(@NotNull File file) {
        try {
            return new InputStreamReader(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        }
    }

    public static void writeBytes(File file, byte[] content) {
        write(file, Base64.getEncoder().encodeToString(content));
    }

    public static void write(File file, String content) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.print(content);
        } catch (Exception ignore) {
        }
    }

    public static byte[] readBytes(File file) {
        return Base64.getDecoder().decode(read(file));
    }

    public static String read(File file) {
        StringBuilder builder = new StringBuilder();
        try {
            FileReader reader = new FileReader(file);
            while (reader.ready()) {
                char c = (char) reader.read();
                builder.append(c);
            }
            reader.close();
        } catch (IOException ignore) {
        }
        return builder.toString();
    }

    @NotNull
    public static String getContent(@NotNull File file) {
        try {
            FileInputStream stream = new FileInputStream(file);
            Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            BufferedReader input = new BufferedReader(reader);
            StringBuilder builder = new StringBuilder();
            try {
                String line;
                while ((line = input.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    input.close();
                    stream.close();
                } catch (IOException ignore) {
                }
            }
            return builder.toString();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        }
    }

}
