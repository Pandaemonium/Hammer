package com.moderocky.mask;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;

public class Reaction {

    public static final Emote YES;
    public static final Emote NO;
    public static final Emote HYPE_1;
    public static final Emote HYPE_2;
    public static final Emote HYPE_3;
    public static final Emote WARN_1;
    public static final Emote WARN_2;
    public static final Emote WARN_3;
    public static final Emote SOLVED;

    static {
        JDA jda = Hammer.getJda();
        YES = jda.getEmoteById(562353337975046144L);
        NO = jda.getEmoteById(562353345038123038L);
        HYPE_1 = jda.getEmoteById(454327001214615552L);
        HYPE_2 = jda.getEmoteById(591901716740571151L);
        HYPE_3 = jda.getEmoteById(583237592569479168L);
        WARN_1 = jda.getEmoteById(606103342610907136L);
        WARN_2 = jda.getEmoteById(606108903716945920L);
        WARN_3 = jda.getEmoteById(606103341562462220L);
        SOLVED = jda.getEmoteById(606103344188227584L);
    }

}
