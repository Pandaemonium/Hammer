package com.moderocky.mask.command;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Category;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArgCategory implements Argument<Category> {

    private final JDA jda = Hammer.getJda();

    private boolean required = true;
    private String name = "category";

    @NotNull
    @Override
    public Category serialise(String string) {
        return Objects.requireNonNull(jda.getCategoryById(string.startsWith("cat:") ? string.substring(4) : string));
    }

    @Override
    public boolean matches(String string) {
        try {
            return jda.getCategoryById(string.startsWith("cat:") ? string.substring(4) : string) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgCategory setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgCategory setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
