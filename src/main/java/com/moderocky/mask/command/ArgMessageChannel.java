package com.moderocky.mask.command;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArgMessageChannel implements Argument<MessageChannel> {

    private final JDA jda = Hammer.getJda();

    private boolean required = true;
    private String name = "channel";

    @NotNull
    @Override
    public MessageChannel serialise(String string) {
        string = (string.startsWith("ch:") ? string.substring(3) : string);
        MessageChannel channel = jda.getTextChannelById(string);
        if (channel != null) return channel;
        try {
            return jda.retrieveUserById(string).complete().openPrivateChannel().complete();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @Override
    public boolean matches(String string) {
        string = (string.startsWith("ch:") ? string.substring(3) : string);
        try {
            if (jda.getTextChannelById(string) != null) return true;
            return jda.retrieveUserById(string).complete() != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgMessageChannel setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgMessageChannel setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
