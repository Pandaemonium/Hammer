package com.moderocky.mask.command;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArgGuild implements Argument<Guild> {

    private final JDA jda = Hammer.getJda();

    private boolean required = true;
    private String name = "guild";

    @NotNull
    @Override
    public Guild serialise(String string) {
        Guild guild = jda.getGuildById(string.startsWith("g:") ? string.substring(2) : string);
        if (guild != null) return guild;
        throw new NullPointerException();
    }

    @Override
    public boolean matches(String string) {
        try {
            return jda.getGuildById(string.startsWith("g:") ? string.substring(2) : string) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgGuild setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgGuild setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
