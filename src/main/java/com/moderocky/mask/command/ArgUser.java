package com.moderocky.mask.command;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArgUser implements Argument<User> {

    private final JDA jda = Hammer.getJda();

    private boolean required = true;
    private String name = "user";

    @NotNull
    @Override
    public User serialise(String string) {
        if (string.matches("^.+#[0-9][0-9][0-9][1-9]$")) {
            List<String> parts = new ArrayList<>(Arrays.asList(string.split("#")));
            String disc = parts.remove(parts.size() - 1);
            String name = String.join("#", parts);
            User user = jda.getUserByTag(name, disc);
            if (user != null) return user;
        }
        if (string.matches("<@!([0-9]+)>")) {
            string = string.substring(3, string.length() - 1);
        } else if (string.matches("<@([0-9]+)>")) {
            string = string.substring(2, string.length() - 1);
        }
        return jda.retrieveUserById(string).complete();
    }

    @Override
    public boolean matches(String string) {
        if (string.matches("^.+#[0-9][0-9][0-9][1-9]$")) {
            List<String> parts = new ArrayList<>(Arrays.asList(string.split("#")));
            String disc = parts.remove(parts.size() - 1);
            String name = String.join("#", parts);
            return (jda.getUserByTag(name, disc) != null);
        } else {
            if (string.matches("<@!([0-9]+)>")) {
                string = string.substring(3, string.length() - 1);
            } else if (string.matches("<@([0-9]+)>")) {
                string = string.substring(2, string.length() - 1);
            }
            try {
                return Long.parseLong(string) > 0;
            } catch (Throwable throwable) {
                return false;
            }
        }
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgUser setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgUser setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
