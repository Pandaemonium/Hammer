package com.moderocky.mask.command;

import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ArgMessageChannelPlural implements Argument<MessageChannel[]> {

    private final JDA jda = Hammer.getJda();

    private boolean required = true;
    private String name = "channels";

    @NotNull
    @Override
    public MessageChannel @NotNull [] serialise(String string) {
        String[] chs = string.split(",");
        List<MessageChannel> channels = new ArrayList<>();
        try {
            for (String ch : chs) {
                channels.add(jda.getTextChannelById(ch));
            }
        } catch (Throwable ignore) {
        }
        return channels.toArray(new MessageChannel[0]);
    }

    @Override
    public boolean matches(String string) {
        String[] chs = string.split(",");
        try {
            for (String ch : chs) {
                if (jda.getTextChannelById(ch) == null) return false;
            }
            return true;
        } catch (Throwable throwable) {
            return false;
        }
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return true;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgMessageChannelPlural setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgMessageChannelPlural setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
