package com.moderocky.mask.command;

import com.moderocky.mask.api.temporal.Timespan;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgTimespan implements Argument<Timespan> {

    private static final Pattern pattern = Pattern.compile("([1-9][0-9]*?)([smhdwy])");
    private final JDA jda = Hammer.getJda();
    private boolean required = true;
    private String name = "user";

    @NotNull
    @Override
    public Timespan serialise(String string) {
        Matcher matcher = pattern.matcher(string);
        matcher.find();
        long number = Long.parseLong(matcher.group(1));
        char period = matcher.group(2).charAt(0);
        if (period == 's') return new Timespan(Duration.of(number, ChronoUnit.SECONDS));
        else if (period == 'm') return new Timespan(Duration.of(number, ChronoUnit.MINUTES));
        else if (period == 'h') return new Timespan(Duration.of(number, ChronoUnit.HOURS));
        else if (period == 'd') return new Timespan(Duration.of(number, ChronoUnit.DAYS));
        else if (period == 'w') return new Timespan(Duration.of(number, ChronoUnit.WEEKS));
        else if (period == 'y') return new Timespan(Duration.of(number, ChronoUnit.YEARS));
        else return new Timespan(Duration.of(number, ChronoUnit.DAYS));
    }

    @Override
    public boolean matches(String string) {
        return pattern.matcher(string).matches();
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>();
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgTimespan setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgTimespan setLabel(@NotNull String name) {
        this.name = name;
        return this;
    }
}
