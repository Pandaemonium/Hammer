package mx.kenzie.hammer.listener;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.command.Commander;
import dev.moderocky.mirror.Mirror;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.UserData;
import mx.kenzie.hammer.discord.ActivityMonitor;
import mx.kenzie.hammer.discord.ChannelFilter;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.guild.GenericGuildEvent;
import net.dv8tion.jda.api.events.guild.member.*;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.Arrays;

import static mx.kenzie.hammer.Hammer.API;
import static mx.kenzie.hammer.Hammer.THREAD_POOL;

public class Listener extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        Guild guild = event.getGuild();
        GuildData data = Hammer.GUILD_MANAGER.getData(guild);
        if (data.hasFilter(event.getChannel()) && !event.getAuthor().isBot()) {
            ChannelFilter filter = data.getFilter(event.getChannel());
            if (!filter.matches(event.getMessage())) check: {
                if ((filter.exempt && Hammer.API.isStaff(event.getAuthor(), guild))) break check;
                event.getMessage().delete().reason("Message does not meet content filter!").queue();
                return;
            }
        }
        ActivityMonitor.monitor(event, event.getMember());
    }

    @Override
    public void onGuildVoiceJoin(@NotNull GuildVoiceJoinEvent event) {
        ActivityMonitor.monitor(event, event.getMember());
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;
        MessageChannel channel = event.getChannel();
        String string = event.getMessage().getContentRaw();
        if (channel.equals(Hammer.HAMMER.getVisited())) {
            System.out.println(Hammer.ConsoleColour.ANSI_CYAN + event.getAuthor().getName() + ": " + Hammer.ConsoleColour.ANSI_RESET + string);
        }
        if (string.startsWith("/")) {
            if (event.isFromGuild()) {
                GuildData data = Hammer.GUILD_MANAGER.getData(event.getGuild());
                if (!data.allowCommand(event.getChannel())) return;
            }
            MagicStringList list = new MagicStringList(Arrays.asList(string.split(" ")));
            String command = list.remove(0).substring(1);
            if (Hammer.COMMANDS.containsKey(command.toLowerCase())) {
                Commander<MessageReceivedEvent> commander = Hammer.COMMANDS.get(command.toLowerCase());
                event.getChannel().sendTyping().queue(null, null);
                THREAD_POOL.execute(() -> {
                    try {
                        commander.execute(event, list.toArray(new String[0]));
                    } catch (Throwable throwable) {
                        try {
                            if (Hammer.DEBUG) throwable.printStackTrace();
                            channel.sendMessage(Reaction.NO.getAsMention() + " Unable to execute command!\n`" + throwable.getMessage() + "`")
                                    .queue(message -> message.addReaction(Reaction.NO).queue());
                        } catch (Throwable thr) {
                            if (Hammer.DEBUG) thr.printStackTrace();
                        }
                    }
                });
            } else if (event.isFromGuild()) {
                GuildData data = Hammer.GUILD_MANAGER.getData(event.getGuild());
                if (data.hasCommand(string)) API.sendMessage(channel, event.getAuthor(), data.getCommand(string))
                        .queue(API.deleteButton(), API.error(channel));
            }
        }
    }

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        SelfUser self = event.getJDA().getSelfUser();
        if (event.getUserId().equalsIgnoreCase(self.getId())) return;
        if (event.getReactionEmote().isEmote()) {
            event.getChannel().retrieveMessageById(event.getMessageId()).queue(message -> {
                if (!(message.getAuthor().getId().equalsIgnoreCase(self.getId()) || message.isWebhookMessage())) return;
                if (event.getReactionEmote().getEmote().getId().equalsIgnoreCase(Reaction.NO.getId())) {
                    message.suppressEmbeds(true).reason("Query complete.").queue();
                    for (MessageReaction reaction : message.getReactions()) {
                        if (reaction.getReactionEmote().isEmote() && reaction.getReactionEmote().getEmote().getId().equalsIgnoreCase(Reaction.NO.getId()))
                            reaction.clearReactions().queue();
                    }
                    Hammer.API.undeleteButton().accept(message);
                } else if (event.getReactionEmote().getEmote().getId().equalsIgnoreCase(Reaction.YES.getId())) {
                    if (!message.getAuthor().getId().equalsIgnoreCase(self.getId())) return;
                    message.suppressEmbeds(false).reason("Query returned.").queue();
                    for (MessageReaction reaction : message.getReactions()) {
                        if (reaction.getReactionEmote().isEmote() && reaction.getReactionEmote().getEmote().getId().equalsIgnoreCase(Reaction.YES.getId()))
                            reaction.clearReactions().queue();
                    }
                    Hammer.API.deleteButton().accept(message);
                }
            });
        }
    }

    @Override
    public void onGuildMemberRemove(@Nonnull GuildMemberRemoveEvent event) {
        Guild guild = event.getGuild();
        Member member = event.getMember();
        if (member == null) return;
        ActivityMonitor.clearActivity(event.getUser(), guild);
        saveRoles(guild, member);
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        ActivityMonitor.monitor(event, event.getMember());
        Guild guild = event.getGuild();
        Member member = event.getMember();
        GuildData guildData = Hammer.GUILD_MANAGER.getData(guild);
        if (guildData.restoreRoles) {
            User user = event.getUser();
            UserData data = Hammer.USER_MANAGER.getData(user);
            JsonObject gobj = (JsonObject) data.getObject().get("guild_" + guild.getId());
            if (gobj != null && gobj.has("roles")) {
                JsonArray array = gobj.getAsJsonArray("roles");
                for (JsonElement element : array) {
                    Role role = guild.getRoleById(element.getAsString());
                    if (role != null) guild.addRoleToMember(member, role).reason("Reassigning previous roles.").queue();
                }
            }
        }
        if (guildData.joinRole != null) check:{
            if (member.getRoles().contains(guildData.joinRole)) break check;
            guild.addRoleToMember(member, guildData.joinRole)
                    .reason("Automatic role assigned.")
                    .queue(null, null);
        }
    }

    @Override
    public void onGenericGuildMember(@Nonnull GenericGuildMemberEvent event) {
        if (event instanceof GuildMemberRoleRemoveEvent || event instanceof GuildMemberRoleAddEvent) {
            Guild guild = event.getGuild();
            User user = event.getUser();
            guild.retrieveMember(user).queue(mem -> saveRoles(guild, mem));
        }
    }

    private void saveRoles(Guild guild, Member member) {
        if (!Hammer.GUILD_MANAGER.getData(guild).restoreRoles) return;
        User user = member.getUser();
        UserData data = Hammer.USER_MANAGER.getData(user);
        MagicStringList roles = new MagicList<>(member.getRoles()).collect(ISnowflake::getId).asMagicStrings();
        JsonArray array = new JsonArray();
        for (String role : roles) {
            array.add(role);
        }
        JsonObject gobj;
        if (data.getObject().has("guild_" + guild.getId())) {
            gobj = (JsonObject) data.getObject().get("guild_" + guild.getId());
        } else {
            gobj = new JsonObject();
        }
        gobj.add("roles", array);
        data.getObject().add("guild_" + guild.getId(), gobj);
        data.save();
    }

}
