package mx.kenzie.hammer;

import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.google.gson.*;
import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.temporal.TemporalAction;
import com.moderocky.mask.api.temporal.Timespan;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import com.moderocky.mask.internal.utility.FileManager;
import dev.moderocky.mirror.Mirror;
import mx.kenzie.hammer.command.*;
import mx.kenzie.hammer.data.*;
import mx.kenzie.hammer.listener.Listener;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.utils.data.DataArray;
import net.dv8tion.jda.api.utils.data.DataObject;
import net.dv8tion.jda.internal.JDAImpl;
import net.dv8tion.jda.internal.handle.GuildSetupController;
import net.dv8tion.jda.internal.handle.GuildSetupNode;
import org.commonmark.node.Node;
import org.jetbrains.annotations.Nullable;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.*;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Hammer extends Thread implements Runnable, Compressive {

    public static final Gson GSON = new Gson();
    public static final GuildManager GUILD_MANAGER = new GuildManager();
    public static final UserManager USER_MANAGER = new UserManager();
    public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(6);
    public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(3);
    public static final MagicList<TemporalAction> SCHEDULED_ACTIONS = new MagicList<>();
    public static final boolean SHARD = false;
    public static final boolean DEBUG = true;
    public static final boolean API_ENABLED = true;
    public static final String API_PATH = "/var/www/api.kenzie.mx/hammer/";
    public static final File API_FOLDER = new File("/var/www/api.kenzie.mx/hammer/");
    public static final Hammer HAMMER = new Hammer();
    public static final DiscordAPI API = new DiscordAPI();
    public static final Map<String, Commander<MessageReceivedEvent>> COMMANDS = new MagicMap<>();
    private static JDA jda;
    private final File schedulerFile = new File("data/scheduled_tasks.json");
    private MessageChannel visited = null;

    private Hammer() {
    }

    public static void main(String[] args) {
        cleanUp();
        HAMMER.start();
        Scanner in = new Scanner(System.in);
        while (true) {
            String input = in.nextLine();
            if (input.equalsIgnoreCase("/stop")) {
                System.out.println("Stopping all processes." + "\n");
                jda.shutdown();
                return;
            } else if (input.equalsIgnoreCase("/update")) {
                System.out.println("Updating API Data." + "\n");
                GUILD_MANAGER.updateStatistics();
                GUILD_MANAGER.updatePublicAPI();
                USER_MANAGER.updatePublicAPI();
            } else if (input.equalsIgnoreCase("/userupdate")) {
                System.out.println("Updating User API Data." + "\n");
                USER_MANAGER.updatePublicAPI();
            } else if (input.equalsIgnoreCase("/userquery")) {
                System.out.println("Querying User API Data." + "\n");
                for (Guild guild : getJda().getGuilds()) {
                    guild.loadMembers(member -> USER_MANAGER.getData(member.getUser()).updateAPI());
                }
                USER_MANAGER.updatePublicAPI();
            } else if (input.startsWith("/makeadmin")) {
                ArgUser argUser = new ArgUser();
                User user = argUser.serialise(input.split(" ")[1]);
                System.out.println("Making " + user.getName() + " an admin.\n");
                UserData data = USER_MANAGER.getData(user);
                data.setAdmin(HAMMER, true);
                data.save();
                data.updateAPI();
            } else if (input.split(" ")[0].equalsIgnoreCase("/visit") && Hammer.DEBUG) {
                String chstr = input.split(" ")[1];
                try {
                    MessageChannel channel = jda.getTextChannelById(chstr);
                    if (channel == null) {
                        ArgUser argUser = new ArgUser();
                        User user;
                        if (argUser.matches(chstr)) {
                            user = argUser.serialise(chstr);
                        } else {
                            List<User> users = jda.getUsersByName(chstr, true);
                            user = users.size() > 0 ? users.get(0) : null;
                        }
                        if (user != null) channel = user.openPrivateChannel().complete();
                    }
                    if (channel != null) {
                        HAMMER.visited = channel;
                        System.out.flush();
                        System.out.println(ConsoleColour.ANSI_YELLOW + "Now visiting: " + ConsoleColour.ANSI_RESET + channel.getName());
                        channel.getHistory().retrievePast(6).queue(messages -> messages.forEach(message -> {
                            System.out.println(ConsoleColour.ANSI_CYAN + message.getAuthor().getName() + ": " + ConsoleColour.ANSI_RESET + message.getContentRaw());
                        }));
                    } else
                        System.out.println("Unable to find channel: " + chstr + ConsoleColour.ANSI_RED + " (Missing)");
                } catch (Exception e) {
                    System.out.println("Unable to find channel: " + chstr + ConsoleColour.ANSI_RED + " (Exception)");
                }
            } else if (HAMMER.getVisited() != null && Hammer.DEBUG) {
                try {
                    HAMMER.getVisited().sendMessage(input).queue(null, throwable -> System.out.println(ConsoleColour.ANSI_RED + "Cannot send message."));
                    System.out.println(ConsoleColour.ANSI_CYAN + "Hammer: " + ConsoleColour.ANSI_RESET + input);
                } catch (Exception e) {
                    System.out.println("Cannot send message: " + ConsoleColour.ANSI_RED + "(" + e.getMessage() + ")");
                }
            }
        }
    }

    private static void cleanUp() {
        cleanPath(new File(Hammer.API_PATH + "punish/"));
        cleanPath(new File(Hammer.API_PATH + "edit/"));
    }

    private static void cleanPath(File file) {
        try {
            Files.walk(file.toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(file1 -> {
                        if (file1.toString().contains(".html") && !file1.toString().contains("template.html") && !file1.toString().contains("error.html") && !file1.toString().contains("complete.html"))
                            file1.delete();
                    });
            file.delete();
        } catch (Throwable ignored) {
        }
        file.mkdirs();
    }

    public static JDA getJda() {
        return jda;
    }

    @Override
    public void run() {
        try {
            JDABuilder builder = JDABuilder.createDefault(token(), GatewayIntent.GUILD_MEMBERS, GatewayIntent.values());
            if (SHARD)
                builder.addEventListeners((EventListener) (event) -> {
                    if (event instanceof ReadyEvent) {
                        JDA.ShardInfo shard = event.getJDA().getShardInfo();
                        System.out.printf(
                                "Shard %d of %d is now ready\n",
                                shard.getShardId() + 1,
                                shard.getShardTotal()
                        );
                    }
                });
            builder.setAutoReconnect(true);
            if (SHARD)
                for (int i = 0; i < 4; i++) {
                    builder.useSharding(i, 4).build();
                }
            builder.addEventListeners(new Listener());
            jda = builder.build().awaitReady();
            jda.getPresence().setActivity(Activity.streaming("/help", "https://twitch.tv/moderocky"));
            register(
                    new HelpCommand(),
                    new InfoCommand(),
                    new GuildCommand(),
                    new UserCommand(),
                    new SettingsCommand(),
                    new ReqCommand(),
                    new GReqCommand(),
                    new GClearCommand(),
                    new LookupCommand(),
                    new DataCommand(),
                    new SetupCommand(),
                    new KenzieCommand(),
                    new WarnCommand(),
                    new MuteCommand(),
                    new BanCommand(),
                    new PurgeCommand(),
                    new UnbanCommand(),
                    new UnmuteCommand(),
                    new WebsiteCommand(),
                    new PunishCommand()
            );
            Interceptor.accept(); // Begin web-connection acceptor.
            for (Guild guild : jda.getGuilds()) {
                GUILD_MANAGER.getData(guild); // START-UP LOADING!
            }
            if (API_ENABLED) {
                API_FOLDER.mkdirs();
                Runnable runnable = () -> {
                    GUILD_MANAGER.updatePublicAPI();
                    USER_MANAGER.updatePublicAPI();
                };
                WebDataManager.process();
                SCHEDULER.scheduleAtFixedRate(runnable, 5, 120, TimeUnit.SECONDS);
                SCHEDULER.scheduleAtFixedRate(GUILD_MANAGER::updateStatistics, 60, 300, TimeUnit.SECONDS);
                SCHEDULER.scheduleAtFixedRate(WebDataManager::process, 20, 300, TimeUnit.SECONDS);
            }
            SCHEDULER.schedule(() -> {
                loadTasks();
                System.out.println("Loading saved tasks.");
            }, 3, TimeUnit.SECONDS);
            SCHEDULER.scheduleAtFixedRate(this::saveTasks, 60, 60, TimeUnit.SECONDS);
        } catch (LoginException | InterruptedException e) {
            System.out.println("Discord integration failed.");
        }
    }

    public void saveTasks() {
        THREAD_POOL.execute(() -> {
            FileManager.putIfAbsent(schedulerFile);
            FileManager.clear(schedulerFile);
            JsonArray array = new JsonArray();
            SCHEDULED_ACTIONS.removeIf(TemporalAction::isComplete);
            for (TemporalAction action : SCHEDULED_ACTIONS) {
                array.add(action.serialise());
            }
            FileManager.write(schedulerFile, array.toString());
        });
    }

    public void loadTasks() {
        try {
            FileManager.putIfAbsent(schedulerFile);
            String string = FileManager.read(schedulerFile);
            JsonArray array;
            if (string.trim().isEmpty())
                array = new JsonArray();
            else
                array = JsonParser.parseString(string).getAsJsonArray();
            SCHEDULED_ACTIONS.clear();
            for (JsonElement element : array) {
                TemporalAction action = TemporalAction.deserialise(element);
                schedule(action);
            }
        } catch (Throwable throwable) {
            if (DEBUG) throwable.printStackTrace();
        }
    }

    public void schedule(TemporalAction action) {
        action.schedule(SCHEDULER);
        if (!action.isComplete()) SCHEDULED_ACTIONS.add(action);
    }

    public MessageChannel getVisited() {
        return visited;
    }

    @SafeVarargs
    private final void register(Commander<MessageReceivedEvent>... commanders) {
        for (Commander<MessageReceivedEvent> commander : commanders) {
            COMMANDS.put(commander.getCommand(), commander);
            for (String alias : commander.getAliases()) {
                COMMANDS.put(alias, commander);
            }
        }
    }

    private String token() throws RuntimeException {
        return getResource("token").trim();
    }

    public String getResource(String key) throws RuntimeException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(key);
        if (stream == null) return null;
        StringBuilder buffer = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
            int c;
            while ((c = reader.read()) != -1) {
                buffer.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return buffer.toString();
    }

    public static class DiscordAPI {

        public JsonObject getSuperficialData(ISnowflake snowflake) {
            JsonObject object = new JsonObject();
            object.addProperty("id", snowflake.getId());
            object.addProperty("id_raw", snowflake.getIdLong());
            return object;
        }

        public JsonObject getSuperficialData(Member member) {
            Color color = new Color(member.getColorRaw());
            int r = color.getRed(), g = color.getGreen(), b = color.getBlue();
            JsonObject object = getSuperficialData(member.getUser());
            object.addProperty("nickname", member.getNickname());
            object.addProperty("color", member.getColorRaw());
            object.addProperty("hex_color", String.format("#%02x%02x%02x", r, g, b));
            return object;
        }

        public JsonObject getSuperficialData(User user) {
            JsonObject object = getSuperficialData((ISnowflake) user);
            object.addProperty("name", user.getName());
            object.addProperty("tag", user.getDiscriminator());
            object.addProperty("avatar_url", user.getEffectiveAvatarUrl());
            object.addProperty("flags", user.getFlagsRaw());
            object.addProperty("badges", user.getFlags().toString());
            object.addProperty("is_bot", user.isBot());
            return object;
        }

        public JsonObject getSuperficialData(GuildChannel channel) {
            JsonObject object = getSuperficialData((ISnowflake) channel);
            object.addProperty("name", channel.getName());
            object.addProperty("type", channel.getType().toString());
//            object.addProperty("type_raw", getChannelType(channel));
            object.addProperty("is_guild", channel.getType().isGuild());
            object.addProperty("has_message", (channel instanceof MessageChannel) && ((MessageChannel) channel).hasLatestMessage());
            object.addProperty("is_voice", (channel instanceof VoiceChannel));
            object.addProperty("position", channel.getPosition());
            object.addProperty("url", "https://canary.discordapp.com/channels/" + channel.getGuild().getId() + "/" + channel.getId() + "/");
            return object;
        }

        public JsonObject getSuperficialData(TextChannel channel) {
            JsonObject object = getSuperficialData((ISnowflake) channel);
            object.addProperty("name", channel.getName());
            object.addProperty("type", channel.getType().toString());
            object.addProperty("type_raw", channel.getType().getId());
            object.addProperty("is_guild", channel.getType().isGuild());
            object.addProperty("has_message", channel.hasLatestMessage());
            return object;
        }

        public JsonObject getSuperficialData(MessageChannel channel) {
            JsonObject object = getSuperficialData((ISnowflake) channel);
            object.addProperty("name", channel.getName());
            object.addProperty("type", channel.getType().toString());
            object.addProperty("type_raw", channel.getType().getId());
            object.addProperty("is_guild", channel.getType().isGuild());
            object.addProperty("has_message", channel.hasLatestMessage());
            object.addProperty("is_voice", (channel instanceof VoiceChannel));
            return object;
        }

        public JsonObject getSuperficialData(Role role) {
            Color color = new Color(role.getColorRaw());
            int r = color.getRed(), g = color.getGreen(), b = color.getBlue();
            JsonObject object = getSuperficialData((ISnowflake) role);
            object.addProperty("name", role.getName());
            object.addProperty("color", role.getColorRaw());
            object.addProperty("hex_color", String.format("#%02x%02x%02x", r, g, b));
            object.addProperty("permissions", role.getPermissions().toString());
            object.addProperty("permissions_raw", role.getPermissionsRaw());
            object.addProperty("position", role.getPosition());
            object.addProperty("hoisted", role.isHoisted());
            object.addProperty("managed", role.isManaged());
            object.addProperty("mentionable", role.isMentionable());
            object.addProperty("public", role.isPublicRole());
            return object;
        }

        public JsonObject getSuperficialData(Guild guild) {
            JsonObject object = getSuperficialData((ISnowflake) guild);
            object.addProperty("name", guild.getName());
            object.addProperty("icon_url", guild.getIconUrl());
            return object;
        }

        public JsonObject getSuperficialData(Message message) {
            JsonObject object = getSuperficialData((ISnowflake) message);
            Node document = HTMLScanner.PARSER.parse(message.getContentDisplay().replace(System.lineSeparator() + System.lineSeparator(), "<br />")
                    .replace("\n\n", "<br />").replace("\\n\\n", "<br />").replace(System.lineSeparator(), "<br />")
                    .replace("\n", "<br />").replace("\\n", "<br />"));
            String content = HTMLScanner.RENDERER.render(document);
            object.add("author", getSuperficialData(message.getAuthor()));
            object.addProperty("content_raw", message.getContentRaw());
            object.addProperty("content_stripped", message.getContentStripped());
            object.addProperty("content_display", content);
            object.addProperty("is_crosspost", message.getFlags().contains(Message.MessageFlag.IS_CROSSPOST));
            object.addProperty("crossposted", message.getFlags().contains(Message.MessageFlag.CROSSPOSTED));
            object.addProperty("urgent", message.getFlags().contains(Message.MessageFlag.URGENT));
            object.addProperty("source_deleted", message.getFlags().contains(Message.MessageFlag.SOURCE_MESSAGE_DELETED));
            object.addProperty("url", message.getJumpUrl());
            object.add("flags", new MagicList<>(message.getFlags()).collect(Message.MessageFlag::getValue).toJsonArray(JsonPrimitive::new));
            return object;
        }

        public String format(TemporalAccessor accessor) {
            return format(Instant.from(accessor));
        }

        public String format(Instant instant) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy (HH:mm)");
            return format.format(Date.from(instant));
        }

        public JsonArray getTextChannels(Guild guild) {
            return new MagicList<>(guild.getTextChannels()).collect(this::getSuperficialData).toJsonArray(o -> o);
        }

        public JsonArray getRoles(Guild guild) {
            return new MagicList<>(guild.getRoles()).collect(this::getSuperficialData).toJsonArray(o -> o);
        }

        public JsonArray getRoles(Guild guild, Predicate<Role> filter) {
            MagicList<Role> roles = new MagicList<>(guild.getRoles());
            roles.removeIf(filter);
            return roles.collect(this::getSuperficialData).toJsonArray(o -> o);
        }

        public JsonObject getConcreteData(User user) {
            UserData data = USER_MANAGER.getData(user);
            JsonObject object = data.getObject();
            object.add("activity", data.getActivities());
            object.add("audit", data.getActions().toJsonArray(AuditAction::store));
            return object;
        }

        public String pascalCase(String string) {
            String[] words = string.split("\\s");
            StringBuilder capitaliseWord = new StringBuilder();
            for (String w : words) {
                String first = w.substring(0, 1);
                String afterfirst = w.substring(1);
                capitaliseWord.append(first.toUpperCase()).append(afterfirst.toLowerCase()).append(" ");
            }
            return capitaliseWord.toString().trim();
        }

        public DataObject getDataObject(Guild guild) {
            JDAImpl jda = (JDAImpl) guild.getJDA();
            GuildSetupController setupController = jda.getGuildSetupController();
            GuildSetupNode node = setupController.getSetupNodeById(guild.getIdLong());
            return new Mirror<>(node).<DataObject>field("partialGuild").get();
        }

        public int getChannelType(GuildChannel channel) {
            Guild guild = channel.getGuild();
            DataObject object = getDataObject(guild);
            final DataArray channelArray = object.getArray("channels");
            for (int i = 0; i < channelArray.length(); i++) {
                DataObject channelJson = channelArray.getObject(i);
                final long id = channelJson.getLong("id");
                if (id == channel.getIdLong()) return channelJson.getInt("type");
            }
            return -1;
        }

        public JsonArray getCrossposts(Guild guild, int limit) {
            JsonArray array = new JsonArray();
            int count = 0;
            for (TextChannel channel : guild.getTextChannels()) {
                try {
                    for (Message message : channel.getIterableHistory().limit(12).complete()) {
                        if (!message.getFlags().contains(Message.MessageFlag.CROSSPOSTED)) continue;
                        array.add(getSuperficialData(message));
                        count++;
                        if (count >= limit) return array;
                    }
                    if (count >= limit) return array;
                } catch (Throwable ignore) {
                }
            }
            return array;
        }

        public int getTotalUserCount() {
            return totalCount.get();
        }

        private final ICalculable<Integer> totalCount = new ICalculable<Integer>() {
            @Override
            public void update() {
                int users = 0;
                for (Guild guild : Hammer.getJda().getGuilds()) {
                    users += guild.getMemberCount();
                }
                type = users;
            }
        };

        public int getTotalOnlineCount() {
            return onlineCount.get();
        }

        private final ICalculable<Integer> onlineCount = new ICalculable<Integer>() {
            @Override
            public void update() {
                int users = 0;
                for (Guild guild : Hammer.getJda().getGuilds()) {
                    users += guild.retrieveMetaData().complete().getApproximatePresences();
                }
                type = users;
            }
        };

        public int getTotalGuildCount() {
            return Hammer.getJda().getGuilds().size();
        }

        public Consumer<Throwable> error(MessageChannel channel) {
            return throwable -> {
                try {
                    channel.sendMessage(Reaction.NO.getAsMention() + " An error has occurred!\n`" + throwable.getMessage() + "`")
                            .queue(deleteButton(), thr -> {
                                if (DEBUG) thr.printStackTrace();
                            });
                } catch (Throwable error) {
                    if (DEBUG) error.printStackTrace();
                }
            };
        }

        public Consumer<Message> deleteButton() {
            return message -> {
                if (message.getEmbeds().size() > 0)
                    message.addReaction(Reaction.NO).queueAfter(1, TimeUnit.SECONDS);
            };
        }

        public Consumer<Message> undeleteButton() {
            return message -> {
                if (message.getEmbeds().size() > 0)
                    message.addReaction(Reaction.YES).queueAfter(1, TimeUnit.SECONDS);
            };
        }

        public boolean isBotAdmin(User user) {
            return USER_MANAGER.getData(user).isAdmin();
        }

        public boolean isStaff(User user, Guild guild) {
            return GUILD_MANAGER.getData(guild).isStaff(user);
        }

        public boolean isAdmin(User user, Guild guild) {
            return isBotAdmin(user) || GUILD_MANAGER.getData(guild).isAdmin(user);
        }

        public MessageEmbed getWarning(Guild guild, User actor, String reason) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.YELLOW)
                    .setAuthor("Warning Issued", null, Reaction.WARN_1.getImageUrl())
                    .setDescription(reason != null ? reason : "No reason was given.")
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor.getName(), true)
                    .setFooter("Warnings are guild-specific, at the discretion of guild staff.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getMute(Guild guild, User actor, String reason) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.ORANGE)
                    .setAuthor("Mute Issued", null, Reaction.WARN_2.getImageUrl())
                    .setDescription(reason != null ? reason : "No reason was given.")
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor.getName(), true)
                    .setFooter("Mutes are guild-specific, at the discretion of guild staff.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getTempmute(Guild guild, User actor, String reason, Timespan timespan) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.ORANGE)
                    .setAuthor("Mute Issued", null, Reaction.WARN_2.getImageUrl())
                    .setDescription(reason != null ? reason : "No reason was given.")
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor.getName(), true)
                    .addField("Time", timespan.toString(true), true)
                    .setFooter("Mutes are guild-specific, at the discretion of guild staff.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getUnmute(Guild guild, User actor) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.GREEN)
                    .setAuthor("Unmuted", null, Reaction.SOLVED.getImageUrl())
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor != null ? actor.getName() : "Auto-Expiry", true)
                    .setFooter("Unmutes are guild-specific, at the discretion of guild staff.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getBan(Guild guild, User actor, String reason) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.RED)
                    .setAuthor("Ban Issued", null, Reaction.WARN_3.getImageUrl())
                    .setDescription(reason != null ? reason : "No reason was given.")
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor.getName(), true)
                    .setFooter("Bans are guild-specific, at the discretion of guild staff.\nSome guilds may opt-in for global bans.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getTempban(Guild guild, User actor, String reason, Timespan timespan) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.RED)
                    .setAuthor("Ban Issued", null, Reaction.WARN_3.getImageUrl())
                    .setDescription(reason != null ? reason : "No reason was given.")
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor.getName(), true)
                    .addField("Time", timespan.toString(true), true)
                    .setFooter("Bans are guild-specific, at the discretion of guild staff.\nSome guilds may opt-in for global bans.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public MessageEmbed getUnban(Guild guild, User actor) {
            EmbedBuilder builder = new EmbedBuilder()
                    .setColor(Color.GREEN)
                    .setAuthor("Unbanned", null, Reaction.SOLVED.getImageUrl())
                    .addField("Guild", guild.getName(), true)
                    .addField("Staff", actor != null ? actor.getName() : "Auto-Expiry", true)
                    .setFooter("Unbans are guild-specific, at the discretion of guild staff.\nSome guilds may opt-in for global bans.")
                    .setTimestamp(Instant.now());
            return builder.build();
        }

        public void notGuild(MessageChannel channel) {
            sendMessage(channel, null, new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This command may only be used within a guild.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        }

        public void notAdmin(MessageChannel channel) {
            sendMessage(channel, null, new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This feature may only be used by a guild admin.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        }

        public void notStaff(MessageChannel channel) {
            sendMessage(channel, null, new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This feature may only be used by guild staff.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        }

        public RestAction<Message> sendMessage(MessageChannel channel, User user, Message message) {
            if (channel instanceof TextChannel && user != null) {
                return GUILD_MANAGER.getData(((TextChannel) channel).getGuild()).sendMessage(channel, user, message);
            } else {
                return channel.sendMessage(message);
            }
        }

        public RestAction<Message> sendMessage(MessageChannel channel, User user, String message) {
            if (channel instanceof TextChannel && user != null) {
                return GUILD_MANAGER.getData(((TextChannel) channel).getGuild()).sendMessage(channel, user, new MessageBuilder().setContent(message).build());
            } else {
                return channel.sendMessage(message);
            }
        }

        public RestAction<Message> sendMessage(MessageChannel channel, User user, MessageEmbed message) {
            if (channel instanceof TextChannel && user != null) {
                return GUILD_MANAGER.getData(((TextChannel) channel).getGuild()).sendMessage(channel, user, new MessageBuilder().setEmbed(message).build());
            } else {
                return channel.sendMessage(message);
            }
        }

        public WebhookMessageBuilder clone(Message message, Color color) {
            WebhookMessageBuilder builder = new WebhookMessageBuilder();
            if (message.getEmbeds().size() > 0) {
                for (MessageEmbed embed : message.getEmbeds()) {
                    WebhookEmbedBuilder embedBuilder = new WebhookEmbedBuilder();
                    if (embed.getAuthor() != null)
                        embedBuilder.setAuthor(new WebhookEmbed.EmbedAuthor(embed.getAuthor().getName(), embed.getAuthor().getUrl(), embed.getAuthor().getProxyIconUrl()));
                    if (embed.getDescription() != null)
                        embedBuilder.setDescription(embed.getDescription());
                    if (embed.getImage() != null)
                        embedBuilder.setImageUrl(embed.getImage().getUrl());
                    if (embed.getThumbnail() != null)
                        embedBuilder.setThumbnailUrl(embed.getThumbnail().getUrl());
                    if (embed.getFooter() != null)
                        embedBuilder.setFooter(new WebhookEmbed.EmbedFooter(embed.getFooter().getText(), embed.getFooter().getIconUrl()));
                    if (embed.getTimestamp() != null)
                        embedBuilder.setTimestamp(embed.getTimestamp());
                    for (MessageEmbed.Field field : embed.getFields()) {
                        embedBuilder.addField(new WebhookEmbed.EmbedField(field.isInline(), field.getName() != null ? field.getName() : "", field.getValue() != null ? field.getValue() : ""));
                    }
                    if (color != null) embedBuilder.setColor(color.getRGB());
                    else embedBuilder.setColor(embed.getColorRaw());
                    builder.addEmbeds(embedBuilder.build());
                }
            }
            if (message.getContentRaw().length() > 0)
                builder.setContent(message.getContentRaw());
            return builder;
        }

        public WebhookMessageBuilder clone(Message message) {
            return clone(message, null);
        }

    }

    public static class ConsoleColour {
        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_BLACK = "\u001B[30m";
        public static final String ANSI_RED = "\u001B[31m";
        public static final String ANSI_GREEN = "\u001B[32m";
        public static final String ANSI_YELLOW = "\u001B[33m";
        public static final String ANSI_BLUE = "\u001B[34m";
        public static final String ANSI_PURPLE = "\u001B[35m";
        public static final String ANSI_CYAN = "\u001B[36m";
        public static final String ANSI_WHITE = "\u001B[37m";
    }

    public List<String> list = new ArrayList<String>() {{
        add("hello");
        add("test");
    }};

}
