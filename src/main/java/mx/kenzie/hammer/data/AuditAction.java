package mx.kenzie.hammer.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dev.moderocky.mirror.Mirror;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class AuditAction {

    protected UUID uuid;
    public final Type type;
    public final long time;
    public final long expiry;
    public final long actor;
    public final long target;
    public final long guild;
    public final @Nullable String reason;
    private final Mirror<?> mirror = Mirror.blank();
    public boolean expired;

    public AuditAction(Type type, Instant time, Instant expiry, User actor, User target, Guild guild, @Nullable String reason) {
        this(type, time.getEpochSecond(), expiry != null ? expiry.getEpochSecond() : -1, actor.getIdLong(), target.getIdLong(), guild.getIdLong(), reason);
        expired = false;
    }

    public AuditAction(Type type, long time, long expiry, long actor, long target, long guild, @Nullable String reason) {
        this.uuid = UUID.randomUUID();
        this.type = type;
        this.time = time;
        this.expiry = expiry;
        this.actor = actor;
        this.target = target;
        this.guild = guild;
        this.reason = reason;
        expired = false;
    }

    public AuditAction(JsonElement element) {
        JsonObject object = element.getAsJsonObject();
        uuid = mirror.tryFunc(r -> UUID.fromString(object.get("uuid").getAsString()), UUID.randomUUID());
        type = mirror.tryFunc(r -> Type.valueOf(object.get("type").getAsString()), Type.UNKNOWN);
        time = mirror.tryFunc(r -> object.get("time").getAsLong(), -1L);
        expiry = mirror.tryFunc(r -> object.get("expiry").getAsLong(), -1L);
        actor = mirror.tryFunc(r -> object.get("actor").getAsLong(), -1L);
        target = mirror.tryFunc(r -> object.get("target").getAsLong(), -1L);
        guild = mirror.tryFunc(r -> object.get("guild").getAsLong(), -1L);
        reason = mirror.tryFunc(r -> object.get("reason").getAsString(), null);
        expired = mirror.tryFunc(r -> object.get("expired").getAsBoolean(), false);
    }

    public long getTime() {
        return time;
    }

    public boolean isExpired() {
        if (!expired && (expiry > 0 && expiry < Instant.now().getEpochSecond())) expired = true;
        return expired;
    }

    public MessageEmbed getAsEmbed() {
        User user = Hammer.getJda().retrieveUserById(target).complete();
        User actor = Hammer.getJda().retrieveUserById(this.actor).complete();
        String expires = expiry > 0 ? Hammer.API.format(Instant.ofEpochSecond(expiry)) : "Never";
        return new EmbedBuilder()
                .setTitle(type.name)
                .setDescription(new StringBuilder()
                        .append("**Target:** ").append(user.getName()).append('#').append(user.getDiscriminator()).append(" `").append(user.getId()).append('`').append("\n")
                        .append("**Expiry:** ").append(expires).append("\n")
                        .append("**Reason:** ").append(reason != null ? reason : "None provided.").append("\n")
                        .toString())
                .setColor(type.color)
                .setFooter("Staff: " + actor.getName() + "#" + actor.getDiscriminator(), actor.getEffectiveAvatarUrl())
                .setTimestamp(Instant.now())
                .build();
    }

    public JsonObject store() {
        JsonObject object = new JsonObject();
        object.addProperty("uuid", uuid.toString());
        object.addProperty("type", type.toString());
        object.addProperty("time", time);
        object.addProperty("expiry", expiry);
        object.addProperty("expired", isExpired());
        object.addProperty("actor", actor);
        object.addProperty("target", target);
        object.addProperty("guild", guild);
        object.addProperty("reason", reason != null && reason.trim().isEmpty() ? null : reason);
        return object;
    }

    public JsonObject getForWeb() {
        JsonObject object = new JsonObject();
        object.addProperty("uuid", uuid.toString());
        object.addProperty("expired", isExpired());
        object.addProperty("type", Hammer.API.pascalCase(type.toString()));
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy (HH:mm)");
        if (time < 1) {
            object.addProperty("time", "Unknown");
        } else {
            Date date = Date.from(Instant.ofEpochSecond(time));
            object.addProperty("time", format.format(date));
        }
        if (expiry < 1) {
            object.addProperty("expiry", "Never");
        } else {
            Date date = Date.from(Instant.ofEpochSecond(expiry));
            object.addProperty("expiry", format.format(date));
        }
        object.add("actor", mirror.tryFunc(r -> Hammer.API.getSuperficialData(Hammer.getJda().retrieveUserById(actor).complete()), null));
        object.add("target", mirror.tryFunc(r -> Hammer.API.getSuperficialData(Hammer.getJda().retrieveUserById(target).complete()), null));
        object.add("guild", mirror.tryFunc(r -> Hammer.API.getSuperficialData(Hammer.getJda().getGuildById(guild)), null));
        object.addProperty("reason", reason != null && reason.trim().isEmpty() ? null : reason);
        return object;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuditAction)) return false;
        AuditAction that = (AuditAction) o;
        return time == that.time &&
                uuid == that.uuid &&
                expired == that.expired &&
                expiry == that.expiry &&
                actor == that.actor &&
                target == that.target &&
                guild == that.guild &&
                type == that.type &&
                Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, time, expiry, actor, target, guild, reason, mirror);
    }

    public enum Type {
        MUTE(0, "Mute", new Color(230, 130, 20)),
        BAN(1, "Ban", new Color(250, 20, 20)),
        WARNING(2, "Warning", new Color(230, 200, 20)),
        KICK(3, "Kick", new Color(230, 90, 20)),
        UNMUTE(4, "Unmute", Color.GREEN),
        UNBAN(5, "Unban", Color.GREEN),
        UNWARN(6, "Warn-removal", Color.GREEN),
        UNKNOWN(-1, "Punishment", new Color(230, 180, 20));

        final int index;
        final String name;
        final Color color;

        Type(int i, String name, Color color) {
            index = i;
            this.name = name;
            this.color = color;
        }

    }

}
