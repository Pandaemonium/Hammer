package mx.kenzie.hammer.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.Guild;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class WebDataManager {

    private static final File file = new File(Hammer.API_PATH + "statistics.json");
    private static JsonObject previous;
    public static final String[] MONTHS = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    public static void process() {
        Hammer.THREAD_POOL.execute(() -> {
            FileManager.putIfAbsent(file);
            try {
                previous = JsonParser.parseString(FileManager.read(file)).getAsJsonObject();
            } catch (Throwable throwable) {
                if (Hammer.DEBUG) throwable.printStackTrace();
                previous = new JsonObject();
            }
            final JsonObject object = new JsonObject();
            object.add("monthly_usage", getMonthlyUsage());
            object.add("server_count_weekly", getServerWeekData());
            object.add("user_count", getUserData());
            object.add("user_count_weekly", getUserWeekData());
            object.add("user_count_day", getUser24Hours());
            object.add("online_count_day", getOnline24Hours());
            object.add("region_data", getRegionData());
            object.add("public_data", getFeatureList("NEWS", "Community", "Private"));
            object.add("verified_data", getFeatureList("VERIFIED", "Verified", "Unverified"));
            object.add("commerce_data", getFeatureList("COMMERCE", "Commercial", "Normal"));
            FileManager.write(file, object.toString());
        });
    }

    public static JsonArray createAtomicPieChart(Map<?, AtomicInteger> map) {
        MagicList<JsonObject> objects = new MagicList<>();
        for (Map.Entry<?, AtomicInteger> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", entry.getKey().toString());
            object.addProperty("count", entry.getValue().get());
            objects.add(object);
        }
        return objects.toJsonArray();
    }

    public static JsonArray createPieChart(Map<?, Number> map) {
        MagicList<JsonObject> objects = new MagicList<>();
        for (Map.Entry<?, Number> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", entry.getKey().toString());
            object.addProperty("count", entry.getValue());
            objects.add(object);
        }
        return objects.toJsonArray();
    }

    public static JsonArray createGraph(JsonArray previous, int pool, DateFormat format, Instant cutOff, int calendarField, Number number) {
        MagicList<JsonObject> objects = MagicList.from(previous, JsonElement::getAsJsonObject);
        String stamp = format.format(Date.from(Instant.now()));
        cutOff(objects, cutOff);
        addPrevious(objects, pool, calendarField, format);
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", number);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static void addPrevious(MagicList<JsonObject> array, int pool, int calField, DateFormat format) {
        String[] months = new String[pool];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < pool; i++) {
            String name = format.format(cal.getTime());
            months[i] = name;
            cal.add(calField, -1);
        }
        fixThing(array, "count", months);
    }

    private static void replaceId(MagicList<JsonObject> objects, JsonObject object) {
        for (JsonObject jsonObject : objects) {
            if (jsonObject.get("id").getAsString().equalsIgnoreCase(object.get("id").getAsString())) {
                for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                    jsonObject.add(entry.getKey(), entry.getValue());
                }
            }
        }
    }

    private static void addPrevMonths(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("MMM");
        String[] months = new String[12];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 12; i++) {
            String name = format.format(cal.getTime());
            months[i] = name;
            cal.add(Calendar.MONTH, -1);
        }
        fixThing(array, valKey, months);
    }

    private static void fixThing(MagicList<JsonObject> array, String valKey, String[] strings) {
        for (String s : strings) {
            boolean has = false;
            for (JsonObject object : array) {
                if (object.get("id").getAsString().equalsIgnoreCase(s)) {
                    has = true;
                }
            }
            if (!has) {
                JsonObject object = new JsonObject();
                object.addProperty("id", s);
                object.addProperty(valKey, 0);
                object.addProperty("tsp", Instant.MIN.getEpochSecond());
                array.add(0, object);
            }
        }
    }

    private static void addPrevWeeks(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("ww");
        String[] weeks = new String[52];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 52; i++) {
            String name = format.format(cal.getTime());
            weeks[i] = name;
            cal.add(Calendar.WEEK_OF_YEAR, -1);
        }
        fixThing(array, valKey, weeks);
    }

    private static void addPrevWeekdays(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        String[] weeks = new String[7];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 7; i++) {
            weeks[i] = format.format(cal.getTime());
            cal.add(Calendar.DAY_OF_WEEK, -1);
        }
        fixThing(array, valKey, weeks);
    }

    private static void addPrevMonth(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        String[] days = new String[30];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 30; i++) {
            days[i] = format.format(cal.getTime());
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        fixThing(array, valKey, days);
    }

    private static void addPrev24Hours(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("HH");
        String[] hours = new String[24];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 24; i++) {
            hours[i] = format.format(cal.getTime());
            cal.add(Calendar.HOUR_OF_DAY, -1);
        }
        fixThing(array, valKey, hours);
    }

    private static void addPrev12Hours(MagicList<JsonObject> array, String valKey) {
        SimpleDateFormat format = new SimpleDateFormat("HH");
        String[] hours = new String[12];
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));
        for (int i = 0; i < 12; i++) {
            hours[i] = format.format(cal.getTime());
            cal.add(Calendar.HOUR_OF_DAY, -1);
        }
        fixThing(array, valKey, hours);
        MagicStringList strings = new MagicStringList(hours);
        array.removeIf(object -> !strings.containsIgnoreCase(object.get("id").getAsString()));
    }

    private static void cutOff(MagicList<JsonObject> array, Instant instant) {
        array.removeIf(object -> (Instant.ofEpochSecond(object.get("tsp").getAsLong()).isBefore(instant)));
    }

    private static JsonArray getMonthlyUsage() {
        JsonArray old = previous.get("monthly_usage") instanceof JsonArray ? previous.getAsJsonArray("monthly_usage") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("MMM");
        String stamp = format.format(Date.from(Instant.now()));
        int size = Hammer.API.getTotalGuildCount();
        cutOff(objects, Instant.now().minus(Duration.ofSeconds(31556952L)));
        addPrevMonths(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", size);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getUserWeekData() {
        JsonArray old = previous.get("user_count_weekly") instanceof JsonArray ? previous.getAsJsonArray("user_count_weekly") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        String stamp = format.format(Date.from(Instant.now()));
        int users = Hammer.API.getTotalUserCount();
        cutOff(objects, Instant.now().minus(Duration.ofDays(7)));
        addPrevWeekdays(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", users);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getServerWeekData() {
        JsonArray old = previous.get("server_count_weekly") instanceof JsonArray ? previous.getAsJsonArray("server_count_weekly") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        String stamp = format.format(Date.from(Instant.now()));
        int servers = Hammer.API.getTotalGuildCount();
        cutOff(objects, Instant.now().minus(Duration.ofDays(7)));
        addPrevWeekdays(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", servers);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getUserData() {
        Instant cutOff = Instant.now().minus(Duration.ofSeconds(31556952L));
        JsonArray old = previous.get("user_count") instanceof JsonArray ? previous.getAsJsonArray("user_count") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("ww");
        String stamp = format.format(Date.from(Instant.now()));
        int users = Hammer.API.getTotalUserCount();
        cutOff(objects, Instant.now().minus(Duration.ofSeconds(31556952L)));
        addPrevWeeks(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", users);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getUser24Hours() {
        JsonArray old = previous.get("user_count_day") instanceof JsonArray ? previous.getAsJsonArray("user_count_day") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("HH");
        String stamp = format.format(Date.from(Instant.now()));
        int users = Hammer.API.getTotalUserCount();
        cutOff(objects, Instant.now().minus(Duration.ofDays(1)));
        addPrev24Hours(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", users);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getOnline24Hours() {
        JsonArray old = previous.get("online_count_day") instanceof JsonArray ? previous.getAsJsonArray("online_count_day") : new JsonArray();
        MagicList<JsonObject> objects = MagicList.from(old, JsonElement::getAsJsonObject);
        SimpleDateFormat format = new SimpleDateFormat("HH");
        String stamp = format.format(Date.from(Instant.now()));
        int users = Hammer.API.getTotalOnlineCount();
        cutOff(objects, Instant.now().minus(Duration.ofDays(1)));
        addPrev24Hours(objects, "count");
        JsonObject object = new JsonObject();
        object.addProperty("id", stamp);
        object.addProperty("count", users);
        object.addProperty("tsp", Instant.now().getEpochSecond());
        replaceId(objects, object);
        return objects.toJsonArray();
    }

    private static JsonArray getRegionData() {
        MagicList<JsonObject> objects = new MagicList<>();
        MagicMap<Region, AtomicInteger> map = new MagicMap<>();
        for (Guild guild : Hammer.getJda().getGuilds()) {
            AtomicInteger integer = map.getOrDefault(guild.getRegion(), new AtomicInteger());
            integer.getAndIncrement();
            map.putIfAbsent(guild.getRegion(), integer);
        }
        for (Map.Entry<Region, AtomicInteger> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", entry.getKey().getName());
            object.addProperty("count", entry.getValue().get());
            objects.add(object);
        }
        return objects.toJsonArray();
    }

    private static JsonArray getMFAData() {
        MagicList<JsonObject> objects = new MagicList<>();
        MagicMap<Guild.MFALevel, AtomicInteger> map = new MagicMap<>();
        for (Guild guild : Hammer.getJda().getGuilds()) {
            AtomicInteger integer = map.getOrDefault(guild.getRequiredMFALevel(), new AtomicInteger());
            integer.getAndIncrement();
            map.putIfAbsent(guild.getRequiredMFALevel(), integer);
        }
        for (Map.Entry<Guild.MFALevel, AtomicInteger> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", Hammer.API.pascalCase(entry.getKey().name().replace("_", " ")));
            object.addProperty("count", entry.getValue().get());
            objects.add(object);
        }
        return objects.toJsonArray();
    }

    private static JsonArray getNotificationData() {
        MagicList<JsonObject> objects = new MagicList<>();
        MagicMap<Guild.NotificationLevel, AtomicInteger> map = new MagicMap<>();
        for (Guild guild : Hammer.getJda().getGuilds()) {
            AtomicInteger integer = map.getOrDefault(guild.getDefaultNotificationLevel(), new AtomicInteger());
            integer.getAndIncrement();
            map.putIfAbsent(guild.getDefaultNotificationLevel(), integer);
        }
        for (Map.Entry<Guild.NotificationLevel, AtomicInteger> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", Hammer.API.pascalCase(entry.getKey().name().replace("_", " ")));
            object.addProperty("count", entry.getValue().get());
            objects.add(object);
        }
        return objects.toJsonArray();
    }
    private static JsonArray getFeatureList(String key, String yes, String no) {
        MagicList<JsonObject> objects = new MagicList<>();
        MagicMap<Boolean, AtomicInteger> map = new MagicMap<>();
        for (Guild guild : Hammer.getJda().getGuilds()) {
            AtomicInteger integer = map.getOrDefault(guild.getFeatures().contains(key), new AtomicInteger());
            integer.getAndIncrement();
            map.putIfAbsent(guild.getFeatures().contains(key), integer);
        }
        for (Map.Entry<Boolean, AtomicInteger> entry : map.entrySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("id", entry.getKey() ? yes : no);
            object.addProperty("count", entry.getValue().get());
            objects.add(object);
        }
        return objects.toJsonArray();
    }

}
