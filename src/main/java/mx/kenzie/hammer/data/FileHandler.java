package mx.kenzie.hammer.data;

import com.moderocky.mask.internal.utility.FileManager;

import java.io.File;

public class FileHandler {

    public static void createTempFile(String path, String content) {
        File file = new File(path);
        FileManager.putIfAbsent(file);
        FileManager.write(file, content);
    }

}
