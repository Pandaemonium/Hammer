package mx.kenzie.hammer.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.api.FileManager;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class UserData {

    private final JsonObject object;
    private final User user;
    private final File apiFile;
    private final Map<String, Integer> activityMap = new MagicMap<>();
    private UUID minecraft;
    private boolean admin;

    UserData(User user, JsonObject object) {
        this.apiFile = new File(Hammer.API_PATH + "user/" + user.getId() + ".json");
        this.user = user;
        this.object = object;
        load();
        save();
    }

    public void setEmail(String email) {
        object.addProperty("email", Base64.getEncoder().encodeToString(email.getBytes(StandardCharsets.UTF_8)));
        save();
    }

    public @Nullable String getEmail() {
        if (object.has("email")) {
            return new String(Base64.getDecoder().decode(object.get("email").getAsString())); // Security :)
        }
        return null;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Hammer hammer, boolean boo) {
        admin = boo;
    }

    public void getPublicAPI(Consumer<JsonObject> consumer) {
        JsonObject object = getObject().deepCopy();
        if (object.has("email")) object.remove("email"); // Security :)
        object.add("audit", getActionsForWeb());
        object.add("user", Hammer.API.getSuperficialData(user));
        object.addProperty("mutual_guilds", user.getMutualGuilds().size());
        object.addProperty("creation_date", new Date((user.getIdLong() / 4194304) + 1420070400000L).toGMTString());
        consumer.accept(object);
    }

    public User getUser() {
        return user;
    }

    public void addAction(AuditAction action) {
        MagicList<AuditAction> actions = getActions();
        actions.add(action);
        setActions(actions);
    }

    public void replaceAction(AuditAction action) {
        MagicList<AuditAction> actions = getActions();
        for (int i = 0; i < actions.size(); i++) {
            AuditAction ac = actions.get(i);
            if (ac.uuid == action.uuid) {
                actions.remove(ac);
                actions.add(i, action);
                break;
            }
        }
        setActions(actions);
    }

    public void addActions(AuditAction... actions) {
        MagicList<AuditAction> auditActions = getActions();
        auditActions.addAll(actions);
        setActions(auditActions);
    }

    public void remove(AuditAction action) {
        MagicList<AuditAction> actions = getActions();
        actions.remove(action);
        setActions(actions);
    }

    public void addActions(Collection<AuditAction> actions) {
        MagicList<AuditAction> auditActions = getActions();
        auditActions.addAll(actions);
        setActions(auditActions);
    }

    public AuditAction getAction(Predicate<? super AuditAction> filter) {
        MagicList<AuditAction> actions = getActions();
        for (AuditAction action : actions) {
            if (filter.test(action)) return action;
        }
        return null;
    }

    public AuditAction expireAction(UUID uuid) {
        AuditAction audit = null;
        MagicList<AuditAction> auditActions = getActions();
        for (AuditAction action : auditActions) {
            if (action.uuid.equals(uuid)) {
                action.expired = true;
                audit = action;
            }
        }
        setActions(auditActions);
        return audit;
    }

    public AuditAction getAction(UUID uuid) {
        for (AuditAction action : getActions()) {
            if (action.uuid.equals(uuid)) {
                return action;
            }
        }
        return null;
    }

    public void removeActionIf(Predicate<? super AuditAction> filter) {
        MagicList<AuditAction> actions = getActions();
        actions.removeIf(filter);
        setActions(actions);
    }

    public void removeAction(UUID uuid) {
        removeActionIf(action -> action.uuid == uuid);
    }

    public MagicList<AuditAction> getActions() {
        JsonElement array = object.get("audit");
        if (!(array instanceof JsonArray)) return new MagicList<>();
        return MagicList.from(array.getAsJsonArray(), element -> new AuditAction(element.getAsJsonObject()));
    }

    public void setActions(MagicList<AuditAction> actions) {
        object.add("audit", actions.toJsonArray(AuditAction::store));
        save();
    }

    public MagicList<AuditAction> getActions(Predicate<? super AuditAction> filter) {
        return getActionsWithout(filter.negate());
    }

    public MagicList<AuditAction> getActionsWithout(Predicate<? super AuditAction> filter) {
        JsonElement array = object.get("audit");
        if (!(array instanceof JsonArray)) return new MagicList<>();
        MagicList<AuditAction> list = MagicList.from(array.getAsJsonArray(), element -> new AuditAction(element.getAsJsonObject()));
        list.removeIf(filter);
        return list;
    }

    public MagicList<AuditAction> getActiveActions() {
        return getActionsWithout(AuditAction::isExpired);
    }

    public JsonArray getActionsForWeb() {
        return getActiveActions().toJsonArray(AuditAction::getForWeb);
    }

    public JsonArray getActionHistory(Guild guild) {
        if (guild == null)
            return getActions().toJsonArray(AuditAction::getForWeb);
        else
            return getActions().without(action -> action.guild != guild.getIdLong()).toJsonArray(AuditAction::getForWeb);
    }

    public void updateAPI() {
        if (!Hammer.API_ENABLED) return;
        FileManager.putIfAbsent(apiFile);
        Hammer.THREAD_POOL.execute(() -> getPublicAPI(json -> FileManager.write(apiFile, new JSONObject(json.toString()).toString(2))));
    }

    private JsonObject getGuild(Guild guild) {
        if (object.has("guild_" + guild.getId()))
            return (JsonObject) object.get("guild_" + guild.getId());
        return new JsonObject();
    }

    private void setGuild(Guild guild, JsonObject data) {
        object.add("guild_" + guild.getId(), data);
        save();
    }

    public List<String> getHistory(Guild guild) {
        MagicList<AuditAction> actions = new MagicList<>(getActions());
        actions.removeIf(action -> action.guild != guild.getIdLong());
        actions.sort(Comparator.comparingLong(AuditAction::getTime));
        MagicList<String> history = new MagicList<>();
        for (AuditAction action : actions.size() > 10 ? actions.getLast(10) : actions) {
            if (action.guild != guild.getIdLong()) continue;
            User user = Hammer.getJda().retrieveUserById(action.actor).complete();
            history.add(new StringBuilder()
                    .append(action.isExpired() ? "~~" : "")
                    .append(action.type.name)
                    .append(" (").append(user.getName()).append('#').append(user.getDiscriminator())
                    .append(") ")
                    .append("Reason: ")
                    .append(action.reason)
                    .append(action.isExpired() ? "~~" : "")
                    .toString());
        }
        return history;
    }

    public JsonArray getActivities() {
        JsonArray array = new JsonArray();
        for (Map.Entry<String, Integer> entry : activityMap.entrySet()) {
            Guild guild = user.getJDA().getGuildById(entry.getKey());
            JsonObject object = new JsonObject();
            object.addProperty("id", entry.getKey());
            object.addProperty("name", guild != null ? guild.getName() : "Unknown");
            object.addProperty("score", entry.getValue());
            array.add(object);
        }
        return array;
    }

    public int getActivity(Guild guild) {
        return activityMap.getOrDefault(guild.getId(), 0);
    }

    public void setActivity(Guild guild, int activity) {
        activityMap.put(guild.getId(), activity);
        save();
    }

    public void addActivity(Guild guild, int activity) {
        activityMap.put(guild.getId(), (getActivity(guild) + activity));
        save();
    }

    public void resetActivity(Guild guild) {
        activityMap.put(guild.getId(), 0);
        save();
    }

    public void removeActivity(Guild guild) {
        activityMap.remove(guild.getId());
        save();
    }

    public final JsonObject getObject() {
        object.addProperty("name", user.getName());
        object.addProperty("flags", user.getFlags().toString());
        object.addProperty("tag", user.getDiscriminator());
        object.addProperty("id", user.getId());
        object.addProperty("icon_url", user.getEffectiveAvatarUrl());
        object.addProperty("minecraft", minecraft != null ? minecraft.toString() : null);
        object.addProperty("is_admin", admin);
        object.add("activity", getActivities());
        return object;
    }

    public final void load() {
        minecraft = object.has("minecraft") && object.get("minecraft").isJsonPrimitive() ? UUID.fromString(object.get("minecraft").getAsString()) : null;
        admin = object.has("is_admin") && object.get("is_admin").getAsBoolean();
        if (object.get("activity") instanceof JsonArray) {
            activityMap.clear();
            for (JsonElement element : object.get("activity").getAsJsonArray()) {
                JsonObject act = element.getAsJsonObject();
                activityMap.put(act.get("id").getAsString(), act.get("score").getAsInt());
            }
        }
    }

    public final void save() {
        JsonObject object = getObject();
        Hammer.USER_MANAGER.saveData(user, object);
    }

}
