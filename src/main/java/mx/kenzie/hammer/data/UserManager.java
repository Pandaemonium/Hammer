package mx.kenzie.hammer.data;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.entities.User;

import java.io.File;
import java.util.HashMap;
import java.util.WeakHashMap;
import java.util.function.Consumer;

public class UserManager implements Compressive {

    protected final String folder = "data/user/";
    protected final WeakHashMap<String, UserData> dataHashMap = new WeakHashMap<>();

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public UserManager() {
        new File(folder).mkdirs();
    }

    public void updatePublicAPI() {
        dataHashMap.values().forEach(UserData::updateAPI);
    }

    public UserData getData(User user) {
        if (!dataHashMap.containsKey(user.getId()))
            dataHashMap.put(user.getId(), new UserData(user, getJson(user)));
        return dataHashMap.get(user.getId());
    }

    public UserData getData(String id) {
        User user = Hammer.getJda().retrieveUserById(id).complete();
        return getData(user);
    }

    public UserData getData(long id) {
        User user = Hammer.getJda().retrieveUserById(id).complete();
        return getData(user);
    }

    public void getData(String id, Consumer<UserData> consumer) {
        Hammer.getJda().retrieveUserById(id).queue(user -> {
            if (consumer != null) consumer.accept(getData(user));
        });
    }

    public void getData(long id, Consumer<UserData> consumer) {
        Hammer.getJda().retrieveUserById(id).queue(user -> {
            if (consumer != null) consumer.accept(getData(user));
        });
    }

    public JsonObject getJson(User user) {
        return getJson(user.getIdLong());
    }

    public JsonObject getJson(long id) {
        File file = new File(folder + id + ".json");
        if (file.exists()) {
            String string = null;
            try {
                string = FileManager.read(file);
            } catch (Throwable exception) {
                if (Hammer.DEBUG) exception.printStackTrace();
            }
            if (string != null && !string.isEmpty()) try {
                return (JsonObject) JsonParser.parseString(string);
            } catch (Throwable throwable) {
                if (Hammer.DEBUG) throwable.printStackTrace();
            }
            return new JsonObject();
        } else {
            return new JsonObject();
        }
    }

    public void saveData(User user, JsonObject data) {
        File file = new File(folder + user.getId() + ".json");
        FileManager.putIfAbsent(file);
        FileManager.write(file, data.toString());
    }

    public void writeData(User user, String data) {
        File file = new File(folder + user.getId() + ".json");
        FileManager.putIfAbsent(file);
        FileManager.write(file, data);
        dataHashMap.remove(user.getId());
        getData(user);
    }

}
