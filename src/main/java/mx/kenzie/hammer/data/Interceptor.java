package mx.kenzie.hammer.data;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.api.MagicMap;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.entities.Guild;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class Interceptor {

    private static final String token = Hammer.HAMMER.getResource("api-token");

    public static void accept() {
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(2032), 0);
            server.createContext("/req", new ReqHandler());
            server.setExecutor(null);
            server.start();
        } catch (Throwable throwable) {
            if (Hammer.DEBUG) throwable.printStackTrace();
        }
    }

    static class ReqHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String response = "";
            exchange.sendResponseHeaders(200, response.getBytes().length);
            BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));
            OutputStream stream = exchange.getResponseBody();
            try {
                MagicMap<String, String> map = decode(reader);
                if (map.get("token").equalsIgnoreCase(token)) {
                    if (map.get("action").equalsIgnoreCase("punish")) {
                        JsonObject user = JsonParser.parseString(map.get("user")).getAsJsonObject();
                        JsonObject data = JsonParser.parseString(map.get("data")).getAsJsonObject();
                        UUID uuid = UUID.fromString(data.get("uuid").getAsString());
                        PunishmentManager manager = PunishmentManager.MANAGERS.get(uuid);
                        if (manager == null) {
                            stream.write("ERROR/Invalid punishment source ID.".getBytes(StandardCharsets.UTF_8));
                        } else {
                            String id = user.get("id").getAsString();
                            if (manager.actor.getId().equalsIgnoreCase(id)) {
                                stream.write("SUCCESS".getBytes(StandardCharsets.UTF_8));
                                manager.complete(data);
                                while (PunishmentManager.MANAGERS.containsKey(uuid)) {
                                    PunishmentManager.MANAGERS.remove(uuid);
                                }
                                File jsonFile = new File(Hammer.API_PATH + "punish/" + uuid.toString() + ".json");
                                if (jsonFile.exists()) {
                                    jsonFile.delete();
                                }
                            } else {
                                stream.write("ERROR/Mismatched actor ID.".getBytes(StandardCharsets.UTF_8));
                            }
                        }
                    } else if (map.get("action").equalsIgnoreCase("remove")) {
                        JsonObject user = JsonParser.parseString(map.get("user")).getAsJsonObject();
                        UUID uuid = UUID.fromString(map.get("punish_id"));
                        Hammer.getJda().retrieveUserById(user.get("id").getAsString()).queue(actor -> Hammer.getJda().retrieveUserById(map.get("target")).queue(target -> {
                            UserData userData = Hammer.USER_MANAGER.getData(target);
                            AuditAction action = userData.getAction(uuid);
                            if (action == null) return;
                            if (action.expired) return;
                            Guild guild = Hammer.getJda().getGuildById(action.guild);
                            if (!Hammer.API.isStaff(actor, guild)) return;
                            userData.expireAction(uuid);
                            switch (action.type) {
                                case BAN:
                                    userData.addAction(PunishmentManager.createUnbanAction(actor.getIdLong(), action.target, action.guild));
                                    break;
                                case MUTE:
                                    userData.addAction(PunishmentManager.createUnmuteAction(actor.getIdLong(), action.target, action.guild));
                                    break;
                                case WARNING:
                                    userData.addAction(PunishmentManager.createUnwarnAction(actor.getIdLong(), action.target, action.guild));
                                    break;
                            }
                            PunishmentManager.logRevokeAction(actor, action);
                            PunishmentManager.messageRevokeAction(actor, action);
                        }));
                    } else if (map.get("action").equalsIgnoreCase("email")) {
                        JsonObject user = JsonParser.parseString(map.get("user")).getAsJsonObject();
                        String id = user.get("id").getAsString();
                        String email = user.get("email").getAsString();
                        Hammer.USER_MANAGER.getData(id, data -> data.setEmail(email));
                    } else {
                        System.out.println("Action: " + map.get("action")); // todo
                    }
                } else {
                    stream.write("ERROR/Invalid token.".getBytes(StandardCharsets.UTF_8));
                }
            } catch (Throwable throwable) {
                stream.write("ERROR/Invalid response.".getBytes(StandardCharsets.UTF_8));
            }
            stream.write(response.getBytes());
            stream.close();
        }

        private MagicMap<String, String> decode(BufferedReader reader) throws IOException {
            MagicMap<String, String> map = new MagicMap<>();
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.isEmpty()) break;
                builder.append(line);
                builder.append(System.lineSeparator());
            }
            String string = builder.toString().trim();
            String[] strings = string.split("&");
            for (String s : strings) {
                String[] parts = s.split("=");
                map.put(parts[0], URLDecoder.decode(parts[1], StandardCharsets.UTF_8.name()));
            }
            return map;
        }
    }
}
