package mx.kenzie.hammer.data;

import java.time.Duration;
import java.time.Instant;

public abstract class ICalculable<Type> {

    protected Type type;
    protected Instant last = Instant.MIN;

    public Type get() {
        if (last.isBefore(Instant.now().minus(Duration.ofMinutes(2))))
            update();
        return type;
    }

    public abstract void update();

}
