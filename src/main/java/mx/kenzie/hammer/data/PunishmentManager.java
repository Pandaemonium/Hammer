package mx.kenzie.hammer.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.temporal.Timespan;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.scheduled.UnbanAction;
import mx.kenzie.hammer.scheduled.UnmuteAction;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class PunishmentManager {

    public static final MagicMap<UUID, PunishmentManager> MANAGERS = new MagicMap<>();
    private static final File template = new File(Hammer.API_PATH + "punish/template.html");

    public final GuildData guildData;
    public final UserData target;
    public final User actor;
    public final UUID uuid;
    public boolean complete = false;
    public String reason = null;

    static {
        try {
            Files.walk(new File(Hammer.API_PATH + "punish/").toPath())
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(file1 -> {
                        if (file1.toString().trim().endsWith(".json"))
                            file1.delete();
                    });
        } catch (IOException ignore) {

        }
    }

    public PunishmentManager(User target, Member actor) {
        this.uuid = UUID.randomUUID();
        guildData = Hammer.GUILD_MANAGER.getData(actor.getGuild());
        this.target = Hammer.USER_MANAGER.getData(target);
        this.actor = actor.getUser();
    }

    public void create(Consumer<String> url) {
        JsonObject object = new JsonObject();
        object.addProperty("actor", actor.getIdLong());
        object.addProperty("uuid", uuid.toString());
        object.addProperty("guild", guildData.getGuild().getIdLong());
        object.add("user", target.getObject());
        object.add("audit", target.getActionHistory(guildData.getGuild()));
        object.add("preset", guildData.getPresetRules());
        object.addProperty("reason", reason);
        MANAGERS.put(uuid, this);
        File file = new File(Hammer.API_PATH + "punish/" + uuid.toString() + ".html");
        File jsonFile = new File(Hammer.API_PATH + "punish/" + uuid.toString() + ".json");
        FileManager.putIfAbsent(file);
        FileManager.putIfAbsent(jsonFile);
        FileManager.write(jsonFile, object.toString());
        Hammer.SCHEDULER.schedule(() -> {
            if (file.exists()) file.delete();
            if (jsonFile.exists()) jsonFile.delete();
        }, 5, TimeUnit.MINUTES);
        HTMLScanner scanner = new HTMLScanner(object, file, FileManager.read(template));
        scanner.create();
        url.accept("https://api.kenzie.mx/hammer/punish?id=" + uuid.toString());
        // TODO - IDs now used!
    }

    public void complete(JsonElement callback) {
        JsonObject object = callback.getAsJsonObject();
        Guild guild = guildData.getGuild();
        if (!Hammer.API.isStaff(actor, guild)) return;
        AuditAction.Type type = AuditAction.Type.valueOf(object.get("type").getAsString().toUpperCase());
        int expiry = object.get("expiry").getAsInt();
        String reason = object.get("reason") instanceof JsonPrimitive ? object.get("reason").getAsString() : null;
        boolean purge = object.get("purge").toString().contains("true");
        Timespan timespan = expiry < 1 ? null : new Timespan(expiry, ChronoUnit.DAYS);
        complete(timespan, reason, purge, type);
    }

    public void complete(@Nullable Timespan timespan, String reason, boolean purge, AuditAction.Type type) {
        if (complete) return;
        complete = true;
        MANAGERS.remove(this.uuid);
        Guild guild = guildData.getGuild();
        User user = target.getUser();
        if (!Hammer.API.isStaff(actor, guild)) return;
        Instant now = Instant.now();
        Instant exp = timespan != null ? now.plus(timespan) : null;
        AuditAction action = new AuditAction(type, now, exp, actor, user, guild, reason);
        action.uuid = uuid;
        String expires = exp != null ? Hammer.API.format(exp) : "Never";
        target.addAction(action);
        guildData.log(action.getAsEmbed());
        try {
            if (!user.isBot())
                user.openPrivateChannel().queue(channel -> channel.sendMessage(new EmbedBuilder()
                        .setTitle(type.name)
                        .setDescription(new StringBuilder()
                                .append("**Guild:** ").append(guild.getName()).append("\n")
                                .append("**Staff:** ").append(actor.getName()).append('#').append(actor.getDiscriminator()).append("\n")
                                .append("**Expiry:** ").append(expires).append("\n")
                                .append("**Reason:** ").append(reason != null ? reason : "None provided.").append("\n")
                                .toString())
                        .setColor(type.color)
                        .setFooter("Warnings are guild-specific, at the discretion of guild staff.")
                        .setTimestamp(now)
                        .build()).queue(null, null));
        } catch (Throwable ignore) {
        }
        switch (type) {
            case WARNING:
                break;
            case KICK:
                if (guild.isMember(target.getUser()))
                    guild.retrieveMember(target.getUser()).queue(member -> guild.kick(member).reason(reason).queue());
                break;
            case MUTE:
                Hammer.GUILD_MANAGER.getData(guild).mute(user);
                if (exp != null)
                    Hammer.HAMMER.schedule(new UnmuteAction(exp, user, guild));
                break;
            case BAN:
                guild.ban(user, purge ? 7 : 0, reason).reason(reason).queue();
                if (exp != null)
                    Hammer.HAMMER.schedule(new UnbanAction(exp, user, guild));
                break;
        }
        File file = new File(Hammer.API_PATH + "punish/" + uuid.toString() + ".html");
        if (file.exists()) file.delete();
        target.save();
        target.updateAPI();
        Hammer.HAMMER.saveTasks();
    }

    public static void logRevokeAction(User actor, AuditAction action) {
        Hammer.getJda().retrieveUserById(action.target).queue(target -> {
            Guild guild = Hammer.getJda().getGuildById(action.guild);
            if (guild == null) return;
            GuildData data = Hammer.GUILD_MANAGER.getData(guild);
            data.log(new EmbedBuilder()
                    .setTitle(action.type.name + " Revoked")
                    .setDescription(new StringBuilder()
                            .append("**Target:** ").append(target.getName()).append('#').append(target.getDiscriminator()).append(" `").append(target.getId()).append('`').append("\n")
                            .append("**Reason:** ~~").append(action.reason != null ? action.reason : "None provided.").append("~~\n")
                            .toString())
                    .setColor(Color.GREEN)
                    .setFooter("Staff: " + actor.getName() + "#" + actor.getDiscriminator(), actor.getEffectiveAvatarUrl())
                    .setTimestamp(Instant.now())
                    .build());
        });
    }

    public static void messageRevokeAction(User actor, AuditAction action) {
        try {
            Hammer.getJda().retrieveUserById(action.target).queue(target -> {
                Guild guild = Hammer.getJda().getGuildById(action.guild);
                if (guild == null) return;
                target.openPrivateChannel().queue(channel -> channel.sendMessage(new EmbedBuilder()
                        .setTitle(action.type.name + " Revoked")
                        .setDescription(new StringBuilder()
                                .append("**Guild:** ").append(guild.getName()).append("\n")
                                .append("**Staff:** ").append(actor.getName()).append('#').append(actor.getDiscriminator()).append("\n")
                                .append("**Reason:** ~~").append(action.reason != null ? action.reason : "None provided.").append("~~\n")
                                .toString())
                        .setColor(Color.GREEN)
                        .setFooter("Warnings are guild-specific, at the discretion of guild staff.")
                        .setTimestamp(Instant.now())
                        .build()).queue(null, null));
            });
        } catch (Throwable ignore) {
        }
    }

    public static AuditAction createUnmuteAction(long actor, long target, long guild) {
        return new AuditAction(AuditAction.Type.UNMUTE, Instant.now().getEpochSecond(), -1, actor, target, guild, null);
    }

    public static AuditAction createUnbanAction(long actor, long target, long guild) {
        return new AuditAction(AuditAction.Type.UNBAN, Instant.now().getEpochSecond(), -1, actor, target, guild, null);
    }

    public static AuditAction createUnwarnAction(long actor, long target, long guild) {
        return new AuditAction(AuditAction.Type.UNWARN, Instant.now().getEpochSecond(), -1, actor, target, guild, null);
    }

}
