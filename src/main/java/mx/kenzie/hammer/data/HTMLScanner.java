package mx.kenzie.hammer.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.api.FileManager;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicStringList;
import mx.kenzie.hammer.Hammer;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLScanner {
    public static final File TEMPLATE = new File(Hammer.API_FOLDER, "stats/template.html");
    public static final File USER_TEMPLATE = new File(Hammer.API_FOLDER, "user/template.html");
    public static final Pattern PATTERN = Pattern.compile("json:\\/\\/([a-zA-Z0-9_-]+(?:\\/[a-zA-Z0-9_-]+)*)");
    public static final Pattern ARRAY = Pattern.compile("array:\\/\\/([a-zA-Z0-9_-]+(?:\\/[a-zA-Z0-9_-]+)*)?\\?([a-zA-Z0-9_-]+\\.html)");
    public static final Parser PARSER = Parser.builder().build();
    public static final HtmlRenderer RENDERER = HtmlRenderer.builder().build();
    final File file;
    final JsonObject object;
    final String content;

    public HTMLScanner(JsonObject object, File file, String template) {
        this.object = object;
        this.file = file;
        this.content = template;
    }

    public void create() {
        FileManager.putIfAbsent(file);
        String string = content;
        string = matchArrayTags(string, object);
        string = string.replace("json://*", Base64.getEncoder().encodeToString(object.toString().getBytes(StandardCharsets.UTF_8)));

        for (String tag : getJsonTags(string)) {
            string = string.replace("json://" + tag, retrieve(tag, object));
        }
        FileManager.write(file, string);
    }

    public void destroy() {
        if (file.exists()) file.delete();
    }

    private String replaceJson(String string, JsonObject object) {
        for (String tag : getJsonTags(string)) {
            string = string.replace("json://" + tag, retrieve(tag, object));
        }
        return string;
    }

    private MagicStringList getJsonTags(String string) {
        MagicStringList list = new MagicStringList();
        Matcher matcher = PATTERN.matcher(string);
        while (matcher.find()) {
            list.add(matcher.group(1));
        }
        return list;
    }

    private String matchArrayTags(String string, JsonObject object) {
        MagicMap<String, String> map = new MagicMap<>();
        Matcher matcher = ARRAY.matcher(string);
        while (matcher.find()) {
            map.put(matcher.group(1), matcher.group(2));
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            StringBuilder builder = new StringBuilder();
            String html = Hammer.HAMMER.getResource(entry.getValue());
            JsonArray array = getElement(entry.getKey(), object).getAsJsonArray();
            if (array == null) {
                string = string.replace("array://" + entry.getKey() + "?" + entry.getValue(), "N/A");
                break;
            }
            for (JsonElement element : array) {
                builder.append(replaceJson(html, element.getAsJsonObject()));
            }
            string = string.replace("array://" + entry.getKey() + "?" + entry.getValue(), builder.toString());
        }
        return string;
    }

    private JsonElement getElement(String key, JsonObject object) {
        MagicStringList list = new MagicStringList(key.split("\\/"));
        JsonElement current = object;
        for (String string : list) {
            if (current instanceof JsonObject) current = ((JsonObject) current).get(string);
        }
        return current;
    }

    private String retrieve(String key, JsonObject object) {
        MagicStringList list = new MagicStringList(key.split("\\/"));
        JsonElement current = object;
        for (String string : list) {
            if (current instanceof JsonObject) current = ((JsonObject) current).get(string);
        }
        String last = "" + current;
        if (last.startsWith("\"") && last.endsWith("\"")) {
            last = last.substring(1, last.length() - 1);
        }
        return last.equalsIgnoreCase("null") ? "None" : last;
    }

}
