package mx.kenzie.hammer.data;

import club.minnced.discord.webhook.WebhookClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.FileManager;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicStringList;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.discord.ChannelFilter;
import mx.kenzie.hammer.discord.DummyRestAction;
import mx.kenzie.hammer.discord.JsonMessageConverter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.RestAction;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

public class GuildData {

    private final JsonObject object;
    private final Guild guild;
    private final MagicMap<String, Message> commandMap = new MagicMap<>();
    private final Map<String, ChannelFilter> filterMap = new HashMap<>();
    private final File apiFile;
    public Role staffRole = null;
    public Role muteRole = null;
    public Role joinRole = null;
    public TextChannel logChannel = null;
    public boolean respondInDMs = false;
    public boolean allowAutoMutes = false;
    public boolean hideInvite = false;
    public boolean restoreRoles = false;
    public boolean allowModActions = false;
    public boolean statsAPI = true;
    public boolean monitorActivity = false;
    private JsonObject activity;
    private JsonObject proxy;
    private JsonObject commands;
    private JsonObject stats;
    private JsonArray rules;
    private MagicStringList enabledChannels;
    private MagicStringList disabledChannels;
    private final Stats statistics = new Stats();

    GuildData(Guild guild, JsonObject object) {
        this.guild = guild;
        this.object = object;
        this.proxy = (object.get("proxy") instanceof JsonObject) ? object.getAsJsonObject("proxy") : getDefaultProxy();
        this.commands = (object.get("custom_commands") instanceof JsonObject) ? object.getAsJsonObject("custom_commands") : new JsonObject();
        this.rules = (object.get("rules") instanceof JsonArray) ? object.getAsJsonArray("rules") : getDefaultRules();
        this.enabledChannels = MagicStringList.from((object.get("enabled_channels") instanceof JsonArray) ? object.getAsJsonArray("enabled_channels") : new JsonArray());
        this.disabledChannels = MagicStringList.from((object.get("disabled_channels") instanceof JsonArray) ? object.getAsJsonArray("disabled_channels") : new JsonArray());
        this.apiFile = new File(Hammer.API_PATH + "server/" + guild.getId() + ".json");
        load();
        save();
    }

    private void loadFilters(JsonArray array) {
        filterMap.clear();
        for (JsonElement element : array) {
            JsonObject object = element.getAsJsonObject();
            ChannelFilter filter = new ChannelFilter(object);
            filterMap.put(filter.id, filter);
        }
    }

    public void setup() {
        load(true);
    }

    private void load() {
        load(false);
    }

    private void load(boolean setup) {
        loadConf(object);
        loadStats(object);
        try {
            List<TextChannel> channels = new ArrayList<>(guild.getTextChannelsByName("audit-log", true));
            channels.addAll(guild.getTextChannelsByName("staff-log", true));
            if (object.has("log_channel") && !object.get("log_channel").isJsonNull()) {
                logChannel = guild.getTextChannelById(object.get("log_channel").getAsString());
            } else if (channels.size() > 0) {
                logChannel = channels.get(0);
                object.addProperty("log_channel", logChannel.getId());
                if (setup)
                    log("Set the logging channel to " + logChannel.getAsMention() + "\nYou may wish to adjust this in the config!");
            } else {
                guild.createTextChannel("audit-log")
                        .setTopic("Hammer moderation and guild action audit logs.")
                        .addRolePermissionOverride(guild.getPublicRole().getIdLong(), null, Arrays.asList(Permission.MESSAGE_WRITE, Permission.MESSAGE_READ))
                        .queue(channel -> {
                            logChannel = channel;
                            object.addProperty("log_channel", channel.getId());
                            if (setup)
                                log("Created a new logging channel, " + logChannel.getAsMention() + "\nYou should adjust its permissons/position!");
                            save();
                        });
            }
        } catch (Throwable ignore) {
            logChannel = null;
        }
        try {
            List<Role> roles = new ArrayList<>(guild.getRolesByName("staff", true));
            roles.addAll(guild.getRolesByName("moderator", true));
            if (object.has("staff_role") && !object.get("staff_role").isJsonNull()) {
                staffRole = guild.getRoleById(object.get("staff_role").getAsString());
            } else if (roles.size() > 0) {
                staffRole = roles.get(0);
                object.addProperty("staff_role", staffRole.getId());
                if (setup)
                    log("Set the 'staff' role to " + staffRole.getName() + "\nYou may wish to adjust this in the config!");
            } else {
                if (setup)
                    log("No 'staff' role has been configured.\nUsers with the 'KICK_MEMBERS' permission will be treated as staff.");
            }
        } catch (Throwable ignore) {
            staffRole = null;
        }
        try {
            List<Role> roles = guild.getRolesByName("muted", true);
            if (object.has("mute_role") && !object.get("mute_role").isJsonNull()) {
                muteRole = guild.getRoleById(object.get("mute_role").getAsString());
            } else if (roles.size() > 0) {
                muteRole = roles.get(0);
                object.addProperty("mute_role", muteRole.getId());
                if (setup)
                    log("Set the 'mute' role to " + muteRole.getName() + "\nYou may wish to adjust this in the config!");
            } else {
                if (setup) log("No 'Muted' role has been configured.\nThe `/mute` command will not be functional.");
            }
        } catch (Throwable throwable) {
            muteRole = null;
        }
    }

    public boolean hasChannelFilters() {
        return filterMap.size() > 0;
    }

    public boolean hasFilter(MessageChannel channel) {
        return filterMap.containsKey(channel.getId());
    }

    public boolean allowCommand(MessageChannel channel) {
        String id = channel.getId();
        if (!disabledChannels.isEmpty() && disabledChannels.containsIgnoreCase(id)) return false;
        return (enabledChannels.isEmpty() || enabledChannels.containsIgnoreCase(id));
    }

    public ChannelFilter getFilter(MessageChannel channel) {
        return filterMap.get(channel.getId());
    }

    public final JsonObject getObject() {
        return object;
    }

    public Guild getGuild() {
        return guild;
    }

    public boolean hasMuteRole() {
        return muteRole != null;
    }

    public Role getMuteRole() {
        return muteRole;
    }

    public boolean isStaff(User user) {
        Member member = guild.retrieveMember(user).complete();
        if (member == null) return false;
        if (member.hasPermission(Permission.ADMINISTRATOR) || member.hasPermission(Permission.BAN_MEMBERS)) return true;
        if (staffRole != null) return member.getRoles().contains(staffRole);
        else return member.hasPermission(Permission.KICK_MEMBERS);
    }

    public boolean isAdmin(User user) {
        Member member = guild.retrieveMember(user).complete();
        if (member == null) return false;
        return member.hasPermission(Permission.ADMINISTRATOR);
    }

    public void mute(User user) {
        Role muteRole = this.muteRole != null ? guild.getRoleById(this.muteRole.getIdLong()) : null;
        if (muteRole != null)
            guild.addRoleToMember(user.getIdLong(), muteRole).reason("Muted.").queue(null, throwable -> log("Unable to mute user!\n`" + throwable.getMessage() + "`"));
        else
            log(Reaction.NO.getAsMention() + " Unable to mute user!\n`No mute-role defined.`");
    }

    public void unmute(User user, @Nullable User act) {
        Role muteRole = this.muteRole != null ? guild.getRoleById(this.muteRole.getIdLong()) : null;
        if (muteRole != null)
            guild.removeRoleFromMember(user.getIdLong(), muteRole).reason("Un-muted.").queue(success -> {
                log(new EmbedBuilder()
                        .setTitle("Unmute")
                        .setDescription(new StringBuilder()
                                .append("**Target:** ").append(user.getName()).append('#').append(user.getDiscriminator()).append(" `").append(user.getId()).append('`').append("\n")
                                .toString())
                        .setColor(Color.green)
                        .setFooter("Staff: " + (act != null ? act.getName() + "#" + act.getDiscriminator() : "Automatic Expiry"), act != null ? act.getEffectiveAvatarUrl() : null)
                        .setTimestamp(Instant.now())
                        .build());
            }, throwable -> log("Unable to unmute user!\n`" + throwable.getMessage() + "`"));
        else
            log(Reaction.NO.getAsMention() + " Unable to unmute user!\n`No mute-role defined.`");
    }

    public void log(String string) {
        if (logChannel != null) {
            logChannel.sendMessage(string).queue();
        }
    }

    public void log(MessageEmbed embed) {
        if (logChannel != null) {
            logChannel.sendMessage(embed).queue();
        }
    }

    public void updateAPI() {
        if (!Hammer.API_ENABLED) return;
        FileManager.putIfAbsent(apiFile);
        Hammer.THREAD_POOL.execute(() -> getPublicAPI(json -> {
            FileManager.write(apiFile, new JSONObject(json.toString()).toString(2));
            HTMLScanner scanner = new HTMLScanner(json, new File(Hammer.API_FOLDER, "stats/" + guild.getId() + ".html"), FileManager.getContent(HTMLScanner.TEMPLATE));
            scanner.create();
        }));
    }

    void getPublicAPI(Consumer<JsonObject> complete) {
        JsonObject object = new JsonObject();
        object.addProperty("id", guild.getId());
        object.addProperty("name", guild.getName());
        object.addProperty("member_count", guild.getMemberCount());
        object.add("features", new MagicStringList(guild.getFeatures()).toJsonArray(s -> {
            JsonObject o = new JsonObject();
            o.addProperty("name", s.toLowerCase().replace("_", " "));
            return o;
        }));
        object.addProperty("icon_url", guild.getIconUrl());
        object.addProperty("banner_url", guild.getBannerUrl());
        object.addProperty("splash_url", guild.getSplashUrl());
        object.addProperty("vanity_url", guild.getVanityUrl());
        object.addProperty("description", guild.getDescription());
        object.add("owner", Hammer.API.getSuperficialData(guild.retrieveOwner().complete()));
        object.addProperty("boost_tier", guild.getBoostTier().getKey());
        object.addProperty("boost_count", guild.getBoostCount());
        object.add("preset_rules", getPresetRules());
        JsonArray array = new JsonArray();
        for (Member booster : guild.getBoosters()) {
            array.add(Hammer.API.getSuperficialData(booster));
        }
        object.add("cross_posts", Hammer.API.getCrossposts(guild, 5));
        object.add("store_channels", new MagicList<>(guild.getStoreChannels()).collect(Hammer.API::getSuperficialData).toJsonArray(o -> o));
        object.add("boosters", array);
        object.addProperty("channel_count", guild.getChannels(true).size());
        object.addProperty("max_file_size", guild.getMaxFileSize());
        object.addProperty("max_bitrate", guild.getMaxBitrate());
        object.addProperty("max_emotes", guild.getMaxEmotes());
        object.addProperty("region", guild.getRegionRaw());
        object.addProperty("verification_level", guild.getVerificationLevel().getKey());
        object.addProperty("verification_level_raw", guild.getVerificationLevel().toString().replace("_", " "));
        object.add("statistics", getStats());
        guild.retrieveMetaData().queue(metaData -> {
            object.addProperty("online_count", metaData.getApproximatePresences());
            object.addProperty("max_online", metaData.getPresenceLimit());
            object.addProperty("max_members", metaData.getMemberLimit());
            try {
                guild.retrievePrunableMemberCount(14).queue(integer -> parseMetadata(complete, object, metaData, integer), failure -> {
                    int integer = (guild.getMemberCount() - metaData.getApproximatePresences());
                    parseMetadata(complete, object, metaData, integer);
                });
            } catch (Throwable throwable) {
                int integer = (guild.getMemberCount() - metaData.getApproximatePresences());
                parseMetadata(complete, object, metaData, integer);
            }
        });
    }

    private void parseMetadata(Consumer<JsonObject> complete, JsonObject object, Guild.MetaData metaData, Integer integer) {
        object.addProperty("inactive_members", integer);
        object.addProperty("active_members", guild.getMemberCount() - integer);
        object.addProperty("pct_member_count", (int) Math.ceil(((float) metaData.getApproximateMembers() / metaData.getMemberLimit()) * 100));
        object.addProperty("pct_online_count", (int) Math.ceil(((float) metaData.getApproximatePresences() / metaData.getApproximateMembers()) * 100));
        object.addProperty("pct_inactive_members", (int) Math.ceil((integer / (float) guild.getMemberCount()) * 100));
        object.addProperty("pct_active_members", (int) Math.floor(100 - (integer / (float) guild.getMemberCount()) * 100));
        complete.accept(object);
    }

    public byte[] getConfig() {
        JsonObject config = new JsonObject();
        saveConf(config);
        return new JSONObject(config.toString()).toString(2).getBytes();
    }

    public void setConfig(String string) throws Exception {
        JsonObject config = (JsonObject) JsonParser.parseString(string);
        loadConf(config);
        saveConf(object);
        load(false);
    }

    public JsonObject getActivity() {
        return activity;
    }

    public void updateStats() {
//        ChartManager.MANAGER.getMemberEntryGraph(guild, url -> stats.addProperty("join_graph", url));
//        ChartManager.MANAGER.getMemberStatusChart(guild, url -> stats.addProperty("status_graph", url));
        updateStatistics(this::collectStats);
    }

    public void collectStats() {
        stats.add("status_graph", WebDataManager.createAtomicPieChart(statistics.statusMap.get()));
        {
            JsonArray array = stats.get("online_graph") instanceof JsonArray ? stats.getAsJsonArray("online_graph") : new JsonArray();
            stats.add("online_graph", WebDataManager.createGraph(array, 24, new SimpleDateFormat("HH"), Instant.now().minus(Duration.ofDays(1)), Calendar.HOUR_OF_DAY, statistics.onlineCount.get()));
        }
        {
            JsonArray array = stats.get("user_graph") instanceof JsonArray ? stats.getAsJsonArray("user_graph") : new JsonArray();
            stats.add("user_graph", WebDataManager.createGraph(array, 12, new SimpleDateFormat("MMM"), Instant.now().minus(Duration.ofDays(365)), Calendar.MONTH, statistics.memberCount.get()));
        }
        {
            JsonArray array = stats.get("user_graph_month") instanceof JsonArray ? stats.getAsJsonArray("user_graph_month") : new JsonArray();
            stats.add("user_graph_month", WebDataManager.createGraph(array, 30, new SimpleDateFormat("dd"), Instant.now().minus(Duration.ofDays(30)), Calendar.DAY_OF_MONTH, statistics.memberCount.get()));
        }
        {
            JsonArray array = stats.get("user_graph_day") instanceof JsonArray ? stats.getAsJsonArray("user_graph_day") : new JsonArray();
            stats.add("user_graph_day", WebDataManager.createGraph(array, 24, new SimpleDateFormat("HH"), Instant.now().minus(Duration.ofDays(1)), Calendar.HOUR_OF_DAY, statistics.memberCount.get()));
        }
    }

    public JsonObject getStats() {
        try {
            collectStats();
            save();
        } catch (Throwable ignore) { // Hasn't propagated yet, we can just continue.
        }
        return stats;
    }

    private void saveStats(final JsonObject main) {
        main.add("statistics", getStats());
    }

    private JsonObject loadStats(final JsonObject main) {
        JsonObject object;
        if (main.has("statistics") && main.get("statistics") instanceof JsonObject)
            object = main.get("statistics").getAsJsonObject();
        else
            object = new JsonObject();
        stats = new JsonObject();
        stats.add("join_graph", object.get("join_graph"));
//        stats.add("channel_graph", object.get("channel_graph"));
        stats.add("status_graph", object.get("status_graph"));
        stats.add("online_graph", object.get("online_graph"));
        stats.add("user_graph", object.get("user_graph"));
        stats.add("user_graph_month", object.get("user_graph_month"));
        stats.add("user_graph_day", object.get("user_graph_day"));
        updateStats();
        return object;
    }

    public void updateStatistics(@Nullable Runnable complete) {
        Hammer.THREAD_POOL.execute(() -> {
            statistics.memberCount.update();
            statistics.onlineCount.update();
            statistics.statusMap.update();
            statistics.boostCount.update();
            statistics.hasRoleMap.update();
            if (complete != null)
                Hammer.SCHEDULER.schedule(complete, 10, TimeUnit.SECONDS);
        });
    }

    public JsonArray getPresetRules() {
        return rules;
    }

    public JsonArray getAvailableRoles() {
        return Hammer.API.getRoles(guild, role -> role.isManaged() || role.isPublicRole());
    }

    private JsonArray getFilterArray() {
        JsonArray array = new JsonArray();
        for (Map.Entry<String, ChannelFilter> entry : filterMap.entrySet()) {
            array.add(entry.getValue().save());
        }
        return array;
    }

    private JsonObject saveConf(final JsonObject object) {
        object.addProperty("mute_role", (muteRole != null) ? muteRole.getId() : null);
        object.addProperty("staff_role", (staffRole != null) ? staffRole.getId() : null);
        object.addProperty("role_on_join", (joinRole != null) ? joinRole.getId() : null);
        object.addProperty("log_channel", (logChannel != null) ? logChannel.getId() : null);
        object.addProperty("respond_via_dm", respondInDMs);
        object.addProperty("allow_auto_mutes", allowAutoMutes);
        object.addProperty("allow_moderation_actions", allowModActions);
        object.addProperty("restore_roles_on_rejoin", restoreRoles);
        object.addProperty("hide_guild_invite", hideInvite);
        object.addProperty("public_stats_api", statsAPI);
        object.add("activity", activity);
        object.add("proxy", proxy);
        object.add("preset_rules", rules);
        object.add("custom_commands", commands);
        object.add("enabled_channels", enabledChannels.toJsonArray());
        object.add("disabled_channels", disabledChannels.toJsonArray());
        object.add("channel_filters", getFilterArray());
        return object;
    }

    private JsonObject loadConf(JsonObject object) {
        respondInDMs = object.has("respond_via_dm") && object.get("respond_via_dm").getAsBoolean();
        allowAutoMutes = object.has("allow_auto_mutes") && object.get("allow_auto_mutes").getAsBoolean();
        allowModActions = object.has("allow_moderation_actions") && object.get("allow_moderation_actions").getAsBoolean();
        hideInvite = object.has("hide_guild_invite") && object.get("hide_guild_invite").getAsBoolean();
        statsAPI = object.has("public_stats_api") && object.get("public_stats_api").getAsBoolean();
        restoreRoles = object.has("restore_roles_on_rejoin") && object.get("restore_roles_on_rejoin").getAsBoolean();
        monitorActivity = object.has("monitor_activity") && object.get("monitor_activity").getAsBoolean();
        joinRole = (object.has("role_on_join") && !object.get("role_on_join").isJsonNull()) ? guild.getRoleById(object.get("role_on_join").getAsString()) : null;
        logChannel = (object.has("log_channel") && !object.get("log_channel").isJsonNull()) ? guild.getTextChannelById(object.get("log_channel").getAsString()) : null;
        staffRole = (object.has("staff_role") && !object.get("staff_role").isJsonNull()) ? guild.getRoleById(object.get("staff_role").getAsString()) : null;
        muteRole = (object.has("mute_role") && !object.get("mute_role").isJsonNull()) ? guild.getRoleById(object.get("mute_role").getAsString()) : null;
        enabledChannels = MagicStringList.from((object.get("enabled_channels") instanceof JsonArray) ? object.getAsJsonArray("enabled_channels") : new JsonArray());
        disabledChannels = MagicStringList.from((object.get("disabled_channels") instanceof JsonArray) ? object.getAsJsonArray("disabled_channels") : new JsonArray());
        loadFilters((object.get("channel_filters") instanceof JsonArray) ? object.getAsJsonArray("channel_filters") : new JsonArray());
        if (object.has("proxy") && object.get("proxy").isJsonObject()) {
            JsonObject prox = object.getAsJsonObject("proxy");
            proxy.add("enabled", prox.get("enabled"));
            proxy.add("name", prox.get("name"));
            proxy.add("avatar", prox.get("avatar"));
            proxy.add("colour", prox.get("colour"));
        } else {
            proxy = getDefaultProxy();
        }
        if (object.has("preset_rules") && object.get("preset_rules").isJsonArray()) {
            rules = object.getAsJsonArray("preset_rules");
        } else {
            rules = getDefaultRules();
        }
        try {
            activity = object.get("activity").getAsJsonObject();
            monitorActivity = activity.get("enabled").getAsBoolean();
        } catch (Throwable throwable) {
            activity = getDefaultActivity();
            monitorActivity = activity.get("enabled").getAsBoolean();
        }
        if (object.has("custom_commands") && object.get("custom_commands").isJsonObject()) {
            JsonObject cmds = object.getAsJsonObject("custom_commands");
            commands = cmds;
            commandMap.clear();
            for (Map.Entry<String, JsonElement> entry : cmds.entrySet()) {
                try {
                    JsonObject cmd = JsonParser.parseString(entry.getValue().getAsString()).getAsJsonObject();
                    Message message = JsonMessageConverter.CONVERTER.toMessage(cmd);
                    commandMap.put(entry.getKey(), message);
                } catch (Throwable throwable) {
                    log("Unable to parse custom command `/" + entry.getKey() + "`\n`" + throwable.getMessage() + "`");
                }
            }
        } else {
            proxy = getDefaultProxy();
        }
        return object;
    }

    private JsonObject getDefaultActivity() {
        JsonObject activity = new JsonObject();
        activity.addProperty("enabled", false);
        activity.addProperty("reset_on_leave", true);
        activity.addProperty("message_sending", 1);
        activity.addProperty("message_ignore_regex", "^[/!.?].*$");
        activity.addProperty("message_min_length", 10);
        activity.addProperty("voice_channel_using", 5);
        activity.addProperty("successful_invite", 10);
        activity.add("role_upgrades", JsonParser.parseString("{\"123456789012345\": 100}"));
        return activity;
    }

    private JsonArray getDefaultRules() {
        JsonArray array = new JsonArray();
        {
            JsonObject object = new JsonObject();
            object.addProperty("name", "Spamming");
            object.addProperty("reason", "Spamming or sending useless messages.");
            array.add(object);
        }
        {
            JsonObject object = new JsonObject();
            object.addProperty("name", "Advertising");
            object.addProperty("reason", "Posting or direct-messaging unsolicited adverts.");
            array.add(object);
        }
        {
            JsonObject object = new JsonObject();
            object.addProperty("name", "Harassment");
            object.addProperty("reason", "Harassing or threatening other users.");
            array.add(object);
        }
        {
            JsonObject object = new JsonObject();
            object.addProperty("name", "Disrespect");
            object.addProperty("reason", "Being rude or disrespectful toward other users.");
            array.add(object);
        }
        {
            JsonObject object = new JsonObject();
            object.addProperty("name", "Irritation");
            object.addProperty("reason", "Excessively pinging or direct-messaging users or roles.");
            array.add(object);
        }
        return array;
    }

    private JsonObject getDefaultProxy() {
        JsonObject proxy = new JsonObject();
        proxy.addProperty("enabled", false);
        proxy.addProperty("name", guild.getJDA().getSelfUser().getName());
        proxy.addProperty("avatar", guild.getJDA().getSelfUser().getEffectiveAvatarUrl());
        proxy.addProperty("colour", new Color(141, 61, 226).getRGB());
        return proxy;
    }

    public RestAction<Message> sendMessage(MessageChannel channel, User target, Message message) {
        if (respondInDMs) {
            return target.openPrivateChannel().complete().sendMessage(message);
        } else if (channel instanceof TextChannel && isProxyEnabled()) {
            TextChannel textChannel = (TextChannel) channel;
            try (WebhookClient client = WebhookClient.withUrl(getWebhook(textChannel))) {
                DummyRestAction<Message> restAction = new DummyRestAction<>(message, guild.getJDA());
                AtomicLong id = new AtomicLong();
                client.send(Hammer.API.clone(message, getProxyColour()).setAvatarUrl(getProxyAvatar()).build()).whenCompleteAsync((readonlyMessage, throwable) -> {
                    id.set(readonlyMessage.getId());
                    Message m = textChannel.retrieveMessageById(readonlyMessage.getId()).complete();
                    restAction.internalComplete(m);
                });
                return restAction;
            }
        } else {
            return channel.sendMessage(message);
        }
    }

    public String getWebhook(TextChannel channel) {
        if (object.has("prx_" + channel.getId()) && !object.get("prx_" + channel.getId()).isJsonNull()) {
            return object.get("prx_" + channel.getId()).getAsString();
        } else {
            String url = channel.createWebhook(getProxyName()).reason("Proxy bot.").complete().getUrl();
            object.addProperty("prx_" + channel.getId(), url);
            save();
            return url;
        }
    }

    public void editSettings(Consumer<String> url) {
        JsonObject object = new JsonObject();
        JsonObject config = new JsonObject();
        saveConf(config);
        object.add("config", config);
        object.add("guild", Hammer.API.getSuperficialData(guild));
        object.addProperty("title", "Guild Settings");
        JsonObject data = new JsonObject();
        data.add("roles", getAvailableRoles());
        data.add("channels", Hammer.API.getTextChannels(guild));
        object.add("data", data);
        if (!Hammer.API_ENABLED) return;
        UUID uuid = UUID.randomUUID();
        File file = new File(Hammer.API_PATH + "edit/" + uuid + ".html");
        FileManager.putIfAbsent(file);
        Hammer.THREAD_POOL.execute(() -> {
            HTMLScanner scanner = new HTMLScanner(object, file, FileManager.getContent(new File(Hammer.API_PATH + "edit/template.html")));
            scanner.create();
            url.accept("https://api.kenzie.mx/hammer/edit/" + uuid);
        });
        Hammer.SCHEDULER.schedule(() -> {
            if (file.exists()) file.delete();
        }, 240, TimeUnit.SECONDS);

    }

    public boolean hasCommands() {
        return commandMap.size() > 0;
    }

    public boolean hasCommand(String string) {
        if (string.startsWith("/")) return commandMap.containsKey(string.substring(1));
        else return commandMap.containsKey(string);
    }

    public Message getCommand(String string) {
        if (string.startsWith("/")) return commandMap.get(string.substring(1));
        else return commandMap.get(string);
    }

    public Color getProxyColour() {
        return (proxy.has("colour") && !proxy.get("colour").isJsonNull()) ? new Color(proxy.get("colour").getAsInt()) : new Color(141, 61, 226);
    }

    public boolean isProxyEnabled() {
        return proxy.has("enabled") && proxy.get("enabled").getAsBoolean();
    }

    public String getProxyName() {
        return proxy.get("name").getAsString();
    }

    public String getProxyAvatar() {
        if (!proxy.has("avatar") || proxy.get("avatar").isJsonNull())
            proxy.add("avatar", getDefaultProxy().get("avatar"));
        return proxy.get("avatar").getAsString();
    }

    public void reset() {
        Hammer.GUILD_MANAGER.reset(guild);
        load();
    }

    public void save() {
        JsonObject object = saveConf(this.object);
        saveStats(object);
        Hammer.GUILD_MANAGER.saveData(guild, object);
    }

    public class Stats {
        final ICalculable<Integer> memberCount = new ICalculable<Integer>() {
            @Override
            public void update() {
                try {
                    type = guild.getMemberCount();
                } catch (Throwable throwable) {
                    guild.retrieveMetaData().queue(metaData -> type = metaData.getApproximateMembers());
                }
            }
        };

        final ICalculable<Integer> onlineCount = new ICalculable<Integer>() {
            @Override
            public void update() {
                try {
                    type = guild.retrieveMetaData().complete().getApproximatePresences();
                } catch (Throwable throwable) {
                    guild.retrieveMetaData().queue(metaData -> type = metaData.getApproximatePresences());
                }
            }
        };

        final ICalculable<Integer> boostCount = new ICalculable<Integer>() {
            @Override
            public void update() {
                AtomicInteger integer = new AtomicInteger();
                guild.loadMembers(member -> {
                    if (member.getTimeBoosted() != null)
                        integer.getAndIncrement();
                }).onSuccess(v -> type = integer.get());
            }
        };

        final ICalculable<Map<String, AtomicInteger>> statusMap = new ICalculable<Map<String, AtomicInteger>>() {
            @Override
            public void update() {
                Map<OnlineStatus, AtomicInteger> map = new MagicMap<>();
                guild.loadMembers(member -> {
                    OnlineStatus status = member.getOnlineStatus();
                    AtomicInteger integer = map.getOrDefault(status, new AtomicInteger());
                    integer.getAndIncrement();
                    map.putIfAbsent(status, integer);
                }).onSuccess(v -> {
                    Map<String, AtomicInteger> finalMap = new HashMap<>();
                    for (Map.Entry<OnlineStatus, AtomicInteger> entry : map.entrySet()) {
                        finalMap.put(Hammer.API.pascalCase(entry.getKey().getKey().replace("_", " ")), entry.getValue());
                    }
                    type = finalMap;
                });
            }
        };

        final ICalculable<Map<String, AtomicInteger>> hasRoleMap = new ICalculable<Map<String, AtomicInteger>>() {
            @Override
            public void update() {
                Map<String, AtomicInteger> map = new MagicMap<>();
                guild.loadMembers(member -> {
                    boolean status = member.getRoles().isEmpty();
                    AtomicInteger integer = map.getOrDefault(status ? "No" : "Yes", new AtomicInteger());
                    integer.getAndIncrement();
                    map.putIfAbsent(status ? "No" : "Yes", integer);
                }).onSuccess(v -> type = map);
            }
        };

        public Stats() {

        }


    }

}
