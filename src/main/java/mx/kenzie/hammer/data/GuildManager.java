package mx.kenzie.hammer.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.api.Compressive;
import com.moderocky.mask.internal.utility.FileManager;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;

import java.io.File;
import java.util.HashMap;
import java.util.WeakHashMap;
import java.util.function.Consumer;

public class GuildManager implements Compressive {
    private final File folder = new File("data/guild/");
    private final WeakHashMap<String, GuildData> dataHashMap = new WeakHashMap<>();

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public GuildManager() {
        folder.mkdirs();
    }

    public void updatePublicAPI() {
        final JDA jda = Hammer.getJda();
        long members = 0;
        for (Guild guild : jda.getGuilds()) {
            members += guild.getMemberCount();
        }
        JsonObject object = new JsonObject();
        JsonArray array = new JsonArray();
        for (Guild guild : Hammer.getJda().getGuilds()) {
            GuildData data = getData(guild);
            JsonObject o = new JsonObject();
            o.addProperty("id", data.getGuild().getId());
            o.addProperty("name", data.getGuild().getName());
            o.addProperty("members", data.getGuild().getMemberCount());
            o.addProperty("icon_url", data.getGuild().getIconUrl());
            o.addProperty("public_api", data.statsAPI);
            o.addProperty("verified", data.getGuild().getFeatures().contains("VERIFIED") ? "<span class=\"fas fa-check-square\"> </span> " : "");
            array.add(o);
        }
        object.addProperty("ping", jda.getGatewayPing());
        object.add("user", Hammer.API.getSuperficialData(jda.getSelfUser()));
        object.addProperty("invite_url", jda.getInviteUrl(Permission.ADMINISTRATOR));
        object.addProperty("total_members", members);
        object.addProperty("total_guilds", jda.getGuilds().size());
        object.addProperty("total_shards", (jda.getShardManager() != null ? jda.getShardManager().getShardsTotal() : 1));
        object.add("guilds", array);
        jda.retrieveUserById("196709350469795841").queue(user -> {
            object.add("owner", Hammer.API.getSuperficialData(user));
            FileHandler.createTempFile(Hammer.API_PATH + "stats/guild-info.json", object.toString());
        });
        forEach(GuildData::updateAPI);
    }

    public void updateStatistics() {
        this.forEach(GuildData::updateStats);
    }

    public GuildData getData(Guild guild) {
        if (!dataHashMap.containsKey(guild.getId()))
            dataHashMap.put(guild.getId(), new GuildData(guild, getJson(guild)));
        return dataHashMap.get(guild.getId());
    }

    void reset(Guild guild) {
        dataHashMap.remove(guild.getId());
        File file = new File(folder, guild.getId() + ".json");
        if (file.exists()) file.delete();
    }

    public void forEach(Consumer<GuildData> consumer) {
        for (GuildData value : dataHashMap.values()) {
            consumer.accept(value);
        }
    }

    JsonObject getJson(Guild guild) {
        File file = new File(folder, guild.getId() + ".json");
        FileManager.putIfAbsent(file);
        String string = null;
        try {
            string = FileManager.read(file);
        } catch (Exception ignore) {
        }
        try {
            if (string == null) return new JsonObject();
            return JsonParser.parseString(string).getAsJsonObject();
        } catch (Throwable throwable) {
            if (Hammer.DEBUG) throwable.printStackTrace();
            return new JsonObject();
        }
    }

    void saveData(Guild guild, JsonObject data) {
        File file = new File(folder, guild.getId() + ".json");
        FileManager.putIfAbsent(file);
        FileManager.write(file, data.toString());
    }

}
