package mx.kenzie.hammer.data;

import com.googlecode.charts4j.*;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicNumberList;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static com.googlecode.charts4j.Color.*;
import static mx.kenzie.hammer.Hammer.ConsoleColour.*;

public class ChartManager {

    public static final ChartManager MANAGER = new ChartManager();
    public static final Color TITLE = Color.newColor("d5acfc");
    public static final Color BACKGROUND = Color.newColor("232529");
    public static final Color AREA = Color.newColor("181a1c");

    private ChartManager() {

    }

    public void getMemberStatusChart(Guild guild, Consumer<String> consumer) {
        Hammer.THREAD_POOL.execute(() -> guild.loadMembers().onSuccess(members -> {
            final MagicMap<OnlineStatus, AtomicInteger> map = new MagicMap<>();
            final List<Slice> slices = new MagicList<>();
            for (OnlineStatus value : OnlineStatus.values()) {
                map.put(value, new AtomicInteger(0));
            }
            for (Member member : members) {
                try {
                    map.get(member.getOnlineStatus()).getAndIncrement();
                } catch (Throwable ignore) {
                    // Unreadable status
                }
            }
            map.removeIf(status -> map.get(status).get() < 1);
            if (map.containsKey(OnlineStatus.ONLINE))
                slices.add(Slice.newSlice(map.get(OnlineStatus.ONLINE).get(), GREEN, properCase("Online")));
            if (map.containsKey(OnlineStatus.OFFLINE))
                slices.add(Slice.newSlice(map.get(OnlineStatus.OFFLINE).get(), DARKGRAY, properCase("Offline")));
            if (map.containsKey(OnlineStatus.DO_NOT_DISTURB))
                slices.add(Slice.newSlice(map.get(OnlineStatus.DO_NOT_DISTURB).get(), RED, properCase("Do Not Disturb")));
            if (map.containsKey(OnlineStatus.IDLE))
                slices.add(Slice.newSlice(map.get(OnlineStatus.IDLE).get(), ORANGE, properCase("Away")));
            if (map.containsKey(OnlineStatus.INVISIBLE))
                slices.add(Slice.newSlice(map.get(OnlineStatus.INVISIBLE).get(), GRAY, properCase("Invisible")));
            PieChart chart = GCharts.newPieChart(slices);
            chart.setSize((750), 400);
//            chart.setTitle("Member Status", TITLE, 24);
            chart.setBackgroundFill(Fills.newSolidFill(BACKGROUND));
            chart.setAreaFill(Fills.newSolidFill(AREA));
            consumer.accept(chart.toURLString());
        }));

    }

    private String properCase(String string) {
        String[] words = string.split("\\s");
        StringBuilder capital = new StringBuilder();
        for (String w : words) {
            String first = w.substring(0, 1);
            String next = w.substring(1);
            capital.append(first.toUpperCase()).append(next.toLowerCase()).append(" ");
        }
        return capital.toString().trim();
    }

    public void getMemberEntryGraph(Guild guild, Consumer<String> consumer) {
        Hammer.THREAD_POOL.execute(() -> guild.loadMembers().onSuccess(members -> {
            final MagicMap<Integer, Integer> map = new MagicMap<>();
            Instant then = guild.getTimeCreated().toInstant();
            int count = 0;
            int week = 0;
            while (then.isBefore(Instant.now())) {
                week++;
                Instant now = then;
                now = now.plus(Duration.ofDays(7));
                for (Member member : members) {
                    if (member.getTimeJoined().toInstant().isBefore(now) && member.getTimeJoined().toInstant().isAfter(then))
                        count++;
                }
                map.put(week, count);
                then = now;
            }
            Integer[] integers = new ArrayList<>(map.values()).toArray(new Integer[0]);
            double[] values = new double[integers.length];
            for (int i = 0; i < integers.length; i++) {
                values[i] = integers[i];
            }
            Data data = DataUtil.scaleWithinRange(0, values[values.length - 1] + 50, values);
            Line line = Plots.newLine(data, BLUEVIOLET);
            line.setLineStyle(LineStyle.MEDIUM_LINE);
            LineChart chart = GCharts.newLineChart(line);
            chart.setSize((750), 400);
            AxisStyle axisStyle = AxisStyle.newAxisStyle(TITLE, 15, AxisTextAlignment.CENTER);
            AxisLabels xLabel = AxisLabelsFactory.newAxisLabels("Week", 50.0);
            xLabel.setAxisStyle(axisStyle);
            chart.addXAxisLabels(xLabel);
            chart.addXAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, map.keySet().size()));
            chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, (int) values[values.length - 1] + 50));
//            chart.setTitle("Member Count", TITLE, 24);
            chart.setBackgroundFill(Fills.newSolidFill(BACKGROUND));
            chart.setAreaFill(Fills.newSolidFill(AREA));
            consumer.accept(chart.toURLString());
        }));
    }

    public void getJoinGraph(Guild guild, Consumer<String> consumer) {
        Hammer.THREAD_POOL.execute(() -> guild.retrieveMembers().whenComplete((voided, throwable) -> {
            final MagicMap<Integer, Long> map = new MagicMap<>();
            Instant then = guild.getTimeCreated().toInstant();
            long count = 0;
            MagicList<Member> members = new MagicList<>(guild.getMembers());

            int i = 0;
            while (then.isBefore(Instant.now())) {
                i++;
                Instant n = then.plus(Duration.ofDays(7));
                for (Member member : members.toArrayList()) {
                    if (member.getTimeJoined().toInstant().isBefore(n) && member.getTimeJoined().toInstant().isAfter(then)) {
                        count++;
                        members.remove(member);
                    }
                }
                map.put(i, count);
                then = n;
            }

            MagicNumberList<Integer> keys = new MagicNumberList<>(map.keySet());
            MagicNumberList<Long> values = new MagicNumberList<>(map.values());

            //Line line = Plots.newLine(DataUtil.scaleWithinRange(0, upscale(values.size(), 1.3), values), BLUEVIOLET);
            XYLine line = Plots.newXYLine(DataUtil.scaleWithinRange(0, upscale(values.size(), 1.3), values), DataUtil.scaleWithinRange(0, keys.size(), keys));
            line.setLineStyle(LineStyle.MEDIUM_LINE);
            //LineChart chart = GCharts.newLineChart(line);
            XYLineChart chart = GCharts.newXYLineChart(line);
            chart.setSize((750), 400);

            AxisStyle axisStyle = AxisStyle.newAxisStyle(TITLE, 15, AxisTextAlignment.CENTER);
            AxisLabels xLabel = AxisLabelsFactory.newAxisLabels("Week", 50.0);
            xLabel.setAxisStyle(axisStyle);
            chart.addXAxisLabels(xLabel);

            chart.addXAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, i));
            chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, (int) upscale(values.size(), 1.3)));

//            chart.setTitle("Member Count", TITLE, 24);
            chart.setBackgroundFill(Fills.newSolidFill(BACKGROUND));
            chart.setAreaFill(Fills.newSolidFill(AREA));

            consumer.accept(chart.toURLString());
        }));
    }

    private void runWeeklyChannelRaster(Consumer<String> consumer, List<GuildChannel> chn) {
        chn.removeIf(guildChannel -> !(guildChannel instanceof MessageChannel));
        MagicList<MessageChannel> channels = new MagicList<>(chn).collect(channel -> (MessageChannel) channel);
        if (channels.size() < 1) return;
        Hammer.THREAD_POOL.execute(() -> {
            final MagicMap<String, Long> mapA = new MagicMap<>();
            final MagicMap<String, Long> mapB = new MagicMap<>();
            final MagicMap<String, Long> mapC = new MagicMap<>();
            for (MessageChannel channel : channels) {
                AtomicLong countA = new AtomicLong(0);
                AtomicLong countB = new AtomicLong(0);
                AtomicLong countC = new AtomicLong(0);
                channel.getIterableHistory().forEachRemaining(message -> {
                    if (message.getTimeCreated().toInstant().isBefore(Instant.now().minus(Duration.ofDays(21))))
                        return false;
                    if (message.getTimeCreated().toInstant().isBefore(Instant.now().minus(Duration.ofDays(14)))) {
                        countC.getAndIncrement();
                    } else if (message.getTimeCreated().toInstant().isBefore(Instant.now().minus(Duration.ofDays(7)))) {
                        countB.getAndIncrement();
                    } else {
                        countA.getAndIncrement();
                    }
                    return true;
                });
                System.out.println(channel.getName() + ": " + ANSI_GREEN + countA.get() + " " + ANSI_YELLOW + countB.get() + " " + ANSI_RED + countC.get() + ANSI_RESET); // TODO
                mapA.put(channel.getName(), countA.get());
                mapB.put(channel.getName(), countB.get());
                mapC.put(channel.getName(), countC.get());
            }
            List<Long> ls = new ArrayList<>();
            ls.addAll(mapA.values());
            ls.addAll(mapB.values());
            ls.addAll(mapC.values());
            long max = getTop(ls);

            List<Long> longA = new MagicList<>(mapA.values()).collect(aLong -> Math.round((aLong / (double) max) * 100));
            List<Long> longB = new MagicList<>(mapB.values()).collect(aLong -> Math.round((aLong / (double) max) * 100));
            List<Long> longC = new MagicList<>(mapC.values()).collect(aLong -> Math.round((aLong / (double) max) * 100));

            BarChartPlot plotA = Plots.newBarChartPlot(Data.newData(longA), GREEN, "~7-0 Days Ago");
            BarChartPlot plotB = Plots.newBarChartPlot(Data.newData(longB), ORANGE, "~14-7 Days Ago");
            BarChartPlot plotC = Plots.newBarChartPlot(Data.newData(longC), RED, "~21-14 Days Ago");
            BarChart chart = GCharts.newBarChart(plotA, plotB, plotC);

            AxisStyle axisStyle = AxisStyle.newAxisStyle(TITLE, 15, AxisTextAlignment.CENTER);
            AxisLabels xLabel = AxisLabelsFactory.newAxisLabels("Channel", 50.0);
            xLabel.setAxisStyle(axisStyle);
            AxisLabels yLabel = AxisLabelsFactory.newAxisLabels("Messages", 50.0);
            yLabel.setAxisStyle(axisStyle);

            chart.addXAxisLabels(AxisLabelsFactory.newAxisLabels(new ArrayList<>(mapA.keySet())));
            chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, getScaleTop(ls)));
            chart.addXAxisLabels(xLabel);
            chart.addYAxisLabels(yLabel);

            chart.setSize((750), 400);
            chart.setMargins(6, 6, 6, 6);
            chart.setDataStacked(false);
            chart.setSpaceBetweenGroupsOfBars(20);
            chart.setSpaceWithinGroupsOfBars(8);
            chart.setBarWidth(BarChart.AUTO_RESIZE);
//            chart.setTitle("Messages (This Week)", TITLE, 24);
            chart.setBackgroundFill(Fills.newSolidFill(BACKGROUND));
            chart.setAreaFill(Fills.newSolidFill(AREA));

            String url = chart.toURLString();
            consumer.accept(url);
        });
    }

    public void getWeeklyMessageCount(GuildChannel[] channels, Consumer<String> consumer) {
        List<GuildChannel> chn = new ArrayList<>(Arrays.asList(channels));
        runWeeklyChannelRaster(consumer, chn);
    }

    private long getTop(Collection<Long> data) {
        MagicList<Long> list = new MagicList<>(data);
        long l = list.get(0);
        for (Long along : list) {
            if (along > l) l = along;
        }
        return l;
    }

    private long getScaleTop(Collection<Long> data) {
        long l = getTop(data);
        return (long) Math.ceil((l + (l * 0.2)));
    }

    private long upscale(double d) {
        return upscale(d, 1.2);
    }

    private long upscale(double d, double factor) {
        return Math.round(d * factor);
    }

}
