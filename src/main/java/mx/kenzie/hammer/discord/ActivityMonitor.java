package mx.kenzie.hammer.discord;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.api.MagicMap;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.UserData;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.GenericGuildEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ActivityMonitor {

    public static void clearActivity(User user, Guild guild) {
        UserData data = Hammer.USER_MANAGER.getData(user);
        GuildData guildData = Hammer.GUILD_MANAGER.getData(guild);
        try {
            if (guildData.getActivity().get("reset_on_leave").getAsBoolean()) {
                data.removeActivity(guild);
            }
        } catch (Throwable ignore) {
        }
    }

    public static <Event extends GenericGuildEvent> void monitor(Event event, Member member) {
        try {
            User user = member.getUser();
            if (user.isBot()) return;
            Guild guild = event.getGuild();
            GuildData data = Hammer.GUILD_MANAGER.getData(guild);
            if (!data.monitorActivity) return;
            UserData userData = Hammer.USER_MANAGER.getData(user);
            JsonObject activity = data.getActivity();
            if (event instanceof GuildMessageReceivedEvent) {
                int amount = activity.get("message_sending").getAsInt();
                int length = activity.get("message_min_length").getAsInt();
                String pattern = activity.get("message_ignore_regex").getAsString();
                String string = ((GuildMessageReceivedEvent) event).getMessage().getContentRaw();
                if (length > 0 && string.length() < length) return;
                if (string.matches(pattern)) return;
                if (amount > 0) {
                    userData.addActivity(guild, amount);
                    updateActivity(data, userData);
                }
            } else if (event instanceof GuildMemberJoinEvent) {
                userData.setActivity(guild, userData.getActivity(guild));
                updateActivity(data, userData);
            } else if (event instanceof GuildVoiceJoinEvent) {
                int amount = activity.get("voice_channel_using").getAsInt();
                if (amount < 1) return;
                Hammer.SCHEDULER.schedule(() -> {
                    if (((GuildVoiceJoinEvent) event).getChannelJoined().getMembers().contains(((GuildVoiceJoinEvent) event).getMember())) {
                        userData.addActivity(guild, amount);
                        updateActivity(data, userData);
                    }
                }, 60, TimeUnit.SECONDS);
            }
        } catch (Throwable ignore) {
        }
    }

    public static void updateActivity(GuildData data, UserData userData) {
        Hammer.THREAD_POOL.execute(() -> {
            if (!data.monitorActivity) return;
            Guild guild = data.getGuild();
            guild.retrieveMember(userData.getUser()).queue(member -> {
                JsonObject activitySect = data.getActivity();
                JsonObject roleUpgrades = activitySect.getAsJsonObject("role_upgrades");
                final Map<Role, Integer> map = new MagicMap<>();
                for (Map.Entry<String, JsonElement> entry : roleUpgrades.entrySet()) {
                    try {
                        Role role = guild.getRoleById(entry.getKey());
                        if (role != null) map.put(role, entry.getValue().getAsInt());
                    } catch (Throwable ignore) {
                    }
                }
                for (Map.Entry<Role, Integer> entry : map.entrySet()) {
                    if (userData.getActivity(guild) > entry.getValue()) {
                        if (member.getRoles().contains(entry.getKey())) continue;
                        guild.addRoleToMember(member, entry.getKey()).queue();
                    }
                }
            });
        });
    }

}
