package mx.kenzie.hammer.discord;

import com.google.gson.JsonObject;
import dev.moderocky.mirror.Mirror;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;

import java.util.regex.Pattern;

public class ChannelFilter {

    public final String id;
    public final Pattern regex;
    public final boolean exempt;
    public final boolean file;

    public ChannelFilter(JsonObject object) {
        id = Mirror.blank().tryFunc(r -> object.get("id").getAsString(), object.get("id").toString());
        regex = Pattern.compile(object.get("regex").getAsString());
        exempt = object.get("staff_exempt").getAsBoolean();
        file = object.get("require_file").getAsBoolean();
    }

    public boolean matches(Message message) {
        if (file && message.getAttachments().size() < 1) return false;
        return (regex.matcher(message.getContentRaw()).matches());
    }

    public boolean isExempt() {
        return exempt;
    }

    public JsonObject save() {
        JsonObject object = new JsonObject();
        object.addProperty("id", id);
        object.addProperty("staff_exempt", exempt);
        object.addProperty("require_file", file);
        object.addProperty("regex", regex.pattern());
        return object;
    }

}
