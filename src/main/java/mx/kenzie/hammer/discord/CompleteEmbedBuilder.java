package mx.kenzie.hammer.discord;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.dv8tion.jda.api.EmbedBuilder;

public class CompleteEmbedBuilder extends EmbedBuilder {

    public static EmbedBuilder from(String string) {
        return JsonMessageConverter.CONVERTER.toEmbedBuilder((JsonObject) JsonParser.parseString(string));
    }

}
