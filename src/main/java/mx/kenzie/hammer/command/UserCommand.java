package mx.kenzie.hammer.command;

import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.UserData;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import static mx.kenzie.hammer.Hammer.API;

public class UserCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("user")
                .arg((event, input) -> {
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    getInfo(user, (User) input[0], channel);
                }, new ArgUser());
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            getInfo(user, user, channel);
        };
    }

    private void getInfo(User query, User user, MessageChannel channel) {
        List<String> info = new MagicList<>(user.getFlags()).collect(User.UserFlag::getName);
        UserData data = Hammer.USER_MANAGER.getData(user);
        data.updateAPI();
        EmbedBuilder builder = new EmbedBuilder()
                .setColor(new Color(141, 61, 226))
                .setAuthor("User Information: " + user.getName() + "#" + user.getDiscriminator())
                .setDescription("In-depth information can be found on the user's profile page [here](https://api.kenzie.mx/hammer/user/" + user.getId() + ").")
                .setThumbnail(user.getAvatarUrl())
                .addField("Special Badges", "" + (info.isEmpty() ? "None :(" : String.join(", ", info)), true)
                .addField("Is a Bot?", "" + (user.isBot() ? "Yes!" : "Nope!"), true)
                .addField("Mutual Guilds", "" + user.getMutualGuilds().size(), true)
                .addField("Creation Date", "" + new Date((user.getIdLong() / 4194304) + 1420070400000L).toGMTString(), true)
                .addField("Bot Admin?", "" + (data.isAdmin() ? "Yes!" : "Nope!"), true)
                .addField("ID", "" + user.getId(), true)
                .setFooter("Queried: " + query.getName() + "#" + query.getDiscriminator())
                .setTimestamp(Instant.now());
        API.sendMessage(channel, user, new MessageBuilder()
                .setEmbed(builder.build())
                .build())
                .queue(API.deleteButton(), API.error(channel));
        data.save();
    }

}
