package mx.kenzie.hammer.command;

import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;

import static mx.kenzie.hammer.Hammer.API;

public class HelpCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("help")
                .arg("basic", event -> {
                    JDA jda = event.getJDA();
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    API.sendMessage(channel, user, new MessageBuilder()
                            .setEmbed(getEmbed(jda, user, HelpType.BASIC))
                            .build())
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                })
                .arg("staff", event -> {
                    JDA jda = event.getJDA();
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    API.sendMessage(channel, user, new MessageBuilder()
                            .setEmbed(getEmbed(jda, user, HelpType.STAFF))
                            .build())
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                })
                .arg("admin", event -> {
                    JDA jda = event.getJDA();
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    API.sendMessage(channel, user, new MessageBuilder()
                            .setEmbed(getEmbed(jda, user, HelpType.ADMIN))
                            .build())
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                });
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            JDA jda = event.getJDA();
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            MessageEmbed embed;
            if (event.isFromGuild()) {
                Member member = event.getMember();
                Guild guild = event.getGuild();
                if (member == null)
                    embed = getEmbed(jda, user, HelpType.BASIC);
                else if (Hammer.API.isAdmin(user, guild))
                    embed = getEmbed(jda, user, HelpType.BASIC, HelpType.STAFF, HelpType.ADMIN);
                else if (Hammer.API.isStaff(user, guild))
                    embed = getEmbed(jda, user, HelpType.BASIC, HelpType.STAFF);
                else
                    embed = getEmbed(jda, user, HelpType.BASIC);
            } else {
                embed = getEmbed(jda, user, HelpType.BASIC);
            }
            API.sendMessage(channel, user, new MessageBuilder()
                    .setEmbed(embed)
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        };
    }

    private MessageEmbed getEmbed(JDA jda, User user, HelpType... helpTypes) {
        String invite = jda.getInviteUrl(Permission.ADMINISTRATOR);
        EmbedBuilder builder = new EmbedBuilder()
                .setColor(new Color(141, 61, 226))
                .setAuthor("The Hammer Help Menu")
                .setThumbnail(jda.getSelfUser().getAvatarUrl())
                .setDescription(Hammer.HAMMER.getResource("help.md") + "\n[Need a Hammer?](" + invite + ")");
        for (HelpType type : helpTypes) {
            type.addField(builder);
        }
        builder
                .addField("Argument Key", "`<user>` = A user ID / Mention / Username#0000\n`<guild>` = A guild ID\n`<role>` = A role ID / Mention\n`<channel>` = A channel ID / Tag", true)
                .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                .setTimestamp(Instant.now());
        return builder.build();
    }

    private enum HelpType {
        BASIC {
            @Override
            void addField(EmbedBuilder builder) {
                builder.addField("Universal Commands", Hammer.HAMMER.getResource("cmds_basic.md"), false);
            }
        }, STAFF {
            @Override
            void addField(EmbedBuilder builder) {
                builder.addField("Guild Staff Commands", Hammer.HAMMER.getResource("cmds_staff.md"), false);
            }
        }, ADMIN {
            @Override
            void addField(EmbedBuilder builder) {
                builder.addField("Guild Admin Commands", Hammer.HAMMER.getResource("cmds_admin.md"), false);
            }
        };

        abstract void addField(EmbedBuilder builder);
    }

}
