package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgCategory;
import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.ArgMessageChannelPlural;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.ChartManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;

import static mx.kenzie.hammer.Hammer.API;

public class DataCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("data")
                .arg("joins", event -> {
                            MessageChannel channel = event.getChannel();
                            if (event.isFromGuild()) {
                                getJoins(channel, event.getGuild(), event.getAuthor());
                            } else {
                                API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                                        .append(Reaction.NO.getAsMention())
                                        .append(" No guild could be retrieved from your entry.")
                                        .build())
                                        .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                            }
                        },
                        arg((event, input) -> {
                            Guild guild = (Guild) input[0];
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            getJoins(channel, guild, user);
                        }, new ArgGuild()))
                .arg("weekly", event -> {
                            MessageChannel channel = event.getChannel();
                            if (event.isFromGuild()) {
                                getWeekly(channel, event.getGuild().getChannels(false).toArray(new GuildChannel[0]), event.getAuthor());
                            } else {
                                API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                                        .append(Reaction.NO.getAsMention())
                                        .append(" No guild could be retrieved from your entry.")
                                        .build())
                                        .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                            }
                        },
                        arg((event, input) -> {
                            Guild guild = (Guild) input[0];
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            getWeekly(channel, guild.getChannels(false).toArray(new GuildChannel[0]), user);
                        }, new ArgGuild()),
                        arg((event, input) -> {
                            Category category = (Category) input[0];
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            getWeekly(channel, category.getChannels().toArray(new GuildChannel[0]), user);
                        }, new ArgCategory()),
                        arg((event, input) -> {
                            MessageChannel[] channels = (MessageChannel[]) input[0];
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            GuildChannel[] chs = new GuildChannel[channels.length];
                            for (int i = 0; i < channels.length; i++) {
                                chs[i] = (GuildChannel) channels[i];
                            }
                            getWeekly(channel, chs, user);
                        }, new ArgMessageChannelPlural())
                );
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            MessageChannel channel = event.getChannel();
            API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This command is for testing purposes only.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        };
    }

    private void getJoins(MessageChannel channel, Guild guild, User user) {
        if (!(Hammer.API.isBotAdmin(user) || Hammer.API.isAdmin(user, guild))) {
            API.sendMessage(channel, user, new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This command may only be used by a guild admin.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
            return;
        }
        preparing(channel);
        ChartManager.MANAGER.getMemberEntryGraph(guild, url -> API.sendMessage(channel, user, new EmbedBuilder()
                .setColor(new Color(141, 61, 226))
                .setAuthor("Data Query Complete", null, guild.getIconUrl())
                .setImage(url)
                .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                .setTimestamp(Instant.now())
                .build())
                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel)));
    }

    private void getWeekly(MessageChannel channel, GuildChannel @NotNull [] channels, User user) {
        if (!(Hammer.API.isBotAdmin(user) || Hammer.API.isAdmin(user, channels[0].getGuild()))) {
            API.sendMessage(channel, user, new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" This command may only be used by a guild admin.")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
            return;
        }
        preparing(channel);
        ChartManager.MANAGER.getWeeklyMessageCount(channels, url -> API.sendMessage(channel, user, new EmbedBuilder()
                .setColor(new Color(141, 61, 226))
                .setAuthor("Data Query Complete", null, channels[0].getGuild().getIconUrl())
                .setImage(url)
                .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                .setTimestamp(Instant.now())
                .build())
                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel)));
    }

    private void preparing(MessageChannel channel) {
        API.sendMessage(channel, null, new MessageBuilder()
                .append("Preparing a graph based on your query.\n")
                .append("This is a rate-based action and could take a long time (20 seconds to 1 hour) to complete.\n")
                .append("Please do not queue up too many requests concurrently, or your access may be limited.")
                .build())
                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
    }

}
