package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.AuditAction;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.PunishmentManager;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;

public class WarnCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("warn");
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) {
                    API.notStaff(channel);
                    return;
                }
                String entry = getInput().trim();
                MagicStringList words = MagicStringList.ofWords(entry);
                if (!(words.size() > 0)) {
                    API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                            .append(Reaction.NO.getAsMention())
                            .append(" No user ID was entered.")
                            .build())
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                    return;
                }
                ArgUser arg = new ArgUser();
                if (arg.matches(words.get(0))) {
                    User target = arg.serialise(words.get(0));
                    String reason = (words.size() > 1) ? String.join(" ", words.from(1)) : null;
                    guild.retrieveMember(user).queue(member -> {
                        PunishmentManager manager = new PunishmentManager(target, member);
                        manager.complete(null, reason, true, AuditAction.Type.WARNING);
                    });
                    API.sendMessage(channel, user, new MessageBuilder()
                            .append(Reaction.YES.getAsMention())
                            .append(" Warning issued!")
                            .build())
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                }
            } else {
                API.sendMessage(channel, user, new MessageBuilder()
                        .append(Reaction.NO.getAsMention())
                        .append(" This command may only be used within a guild.")
                        .build())
                        .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
            }
        };
    }

}
