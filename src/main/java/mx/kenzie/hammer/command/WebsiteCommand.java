package mx.kenzie.hammer.command;

import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.discord.CompleteBuilder;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;

public class WebsiteCommand extends Commander<MessageReceivedEvent> {
    @Override
    public @NotNull CommandImpl create() {
        return command("website");
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            API.sendMessage(channel, user, CompleteBuilder.from(Hammer.HAMMER.getResource("website.json"))
                    .build())
                    .queue(API.deleteButton(), API.error(event.getChannel()));
        };
    }
}
