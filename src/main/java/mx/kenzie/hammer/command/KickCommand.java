package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgStringFinal;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.AuditAction;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.PunishmentManager;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;

public class KickCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("kick")
                .arg((event, input) -> {
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    if (event.isFromGuild()) {
                        Guild guild = event.getGuild();
                        GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                        if (!data.allowModActions) return;
                        if (!data.isStaff(user)) {
                            API.notStaff(channel);
                            return;
                        }
                        User target = (User) input[0];
                        String reason = (String) input[1];
                        guild.retrieveMember(user).queue(member -> {
                            PunishmentManager manager = new PunishmentManager(target, member);
                            manager.complete(null, reason, true, AuditAction.Type.KICK);
                        });
                        API.sendMessage(channel, user, new MessageBuilder()
                                .append(Reaction.YES.getAsMention())
                                .append(" User kicked!")
                                .build())
                                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                    } else {
                        API.sendMessage(channel, user, new MessageBuilder()
                                .append(Reaction.NO.getAsMention())
                                .append(" This command may only be used within a guild.")
                                .build())
                                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                    }
                }, new ArgUser(), new ArgStringFinal().setRequired(false));
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (!event.isFromGuild()) {
                API.sendMessage(channel, user, new MessageBuilder()
                        .append(Reaction.NO.getAsMention())
                        .append(" This command may only be used within a guild.")
                        .build())
                        .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
            }
        };
    }

}
