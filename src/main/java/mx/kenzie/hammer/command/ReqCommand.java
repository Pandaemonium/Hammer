package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.UserData;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static mx.kenzie.hammer.Hammer.API;
import static mx.kenzie.hammer.Hammer.USER_MANAGER;

public class ReqCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("req")
                .arg("user", getDefault(),
                        arg(
                                (event, input) -> {
                                    User user = event.getAuthor();
                                    if (!Hammer.API.isBotAdmin(user)) return;
                                    User target = (User) input[0];
                                    API.sendMessage(event.getChannel(), user, "Sending this user's data via private message.").queue(API.deleteButton(), API.error(event.getChannel()));
                                    user.openPrivateChannel().queue(channel -> data(channel, target), API.error(event.getChannel()));
                                },
                                new ArgUser()))
                .arg("save", getDefault(),
                        arg(
                                (event, input) -> {
                                    User user = event.getAuthor();
                                    if (!Hammer.API.isBotAdmin(user)) return;
                                    List<Message.Attachment> attachments = event.getMessage().getAttachments();
                                    if (attachments.size() < 1) return;
                                    User target = (User) input[0];
                                    MessageChannel channel = event.getChannel();
                                    attachments.get(0).retrieveInputStream().whenCompleteAsync((inputStream, throwable) -> {
                                        try {
                                            String string = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()
                                                    .collect(Collectors.joining(System.lineSeparator()));
                                            USER_MANAGER.writeData(target, string);
                                            API.sendMessage(channel, user, Reaction.YES.getAsMention() + " Saved your data!")
                                                    .queue(API.deleteButton(), API.error(channel));
                                        } catch (Throwable thr) {
                                            Throwable blob = throwable != null ? throwable : thr;
                                            if (Hammer.DEBUG) blob.printStackTrace();
                                            API.sendMessage(channel, user, Reaction.NO.getAsMention() + " An error occurred while saving your data.\n`" + blob.getMessage() + "`")
                                                    .queue(API.deleteButton(), API.error(channel));
                                        }
                                    });
                                    API.sendMessage(event.getChannel(), user, "Saving this user's data.").queue(API.deleteButton(), API.error(event.getChannel()));
                                },
                                new ArgUser()))
                .arg("guild", getDefault(),
                        arg(
                                (event, input) -> {
                                    User user = event.getAuthor();
                                    if (!Hammer.API.isBotAdmin(user)) return;
                                    Guild target = (Guild) input[0];
                                    API.sendMessage(event.getChannel(), user, "Sending this guild's data via private message.").queue(API.deleteButton(), API.error(event.getChannel()));
                                    user.openPrivateChannel().queue(channel -> data(channel, target), API.error(event.getChannel()));
                                },
                                new ArgGuild()));
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            API.sendMessage(event.getChannel(), user, "Sending your data via private message.").queue(API.deleteButton(), API.error(event.getChannel()));
            user.openPrivateChannel().queue(channel -> data(channel, user), API.error(event.getChannel()));
        };
    }

    private void data(MessageChannel channel, User user) {
        UserData data = Hammer.USER_MANAGER.getData(user);
        channel
                .sendFile(data.getObject().toString().getBytes(), "user_data.json")
                .queue(API.deleteButton(), API.error(channel));
    }

    private void data(MessageChannel channel, Guild guild) {
        GuildData data = Hammer.GUILD_MANAGER.getData(guild);
        channel
                .sendFile(data.getObject().toString().getBytes(), "guild_data.json")
                .queue(API.deleteButton(), API.error(channel));
    }
}