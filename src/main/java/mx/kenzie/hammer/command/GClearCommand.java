package mx.kenzie.hammer.command;

import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;

public class GClearCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("gclear")
                .arg("confirm", event -> {
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            if (event.isFromGuild()) {
                                Guild guild = event.getGuild();
                                if (API.isAdmin(user, guild)) {
                                    API.sendMessage(channel, user, new MessageBuilder()
                                            .append("Your guild's data is being purged.")
                                            .build())
                                            .queue(API.deleteButton(), API.error(event.getChannel()));
                                    GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                                    data.reset();
                                } else {
                                    API.notAdmin(channel);
                                }
                            } else {
                                API.notGuild(channel);
                            }
                        },
                        arg((event, input) -> {
                            User user = event.getAuthor();
                            if (!Hammer.API.isBotAdmin(user)) return;
                            Guild guild = (Guild) input[0];
                            API.sendMessage(event.getChannel(), user, new MessageBuilder()
                                    .append("This guild's data is being purged.")
                                    .build())
                                    .queue(API.deleteButton(), API.error(event.getChannel()));
                            GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                            data.reset();
                        }, new ArgGuild())
                );
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                if (API.isAdmin(user, guild)) {
                    API.sendMessage(channel, user, new MessageBuilder()
                            .append("Are you sure you wish to reset this guild's data-store?")
                            .append("\n")
                            .append("*Note: This will not clear user-specific data such as punishment history.*")
                            .append("\n\n")
                            .append("**Confirm?** `/gclear confirm`")
                            .build())
                            .queue(API.deleteButton(), API.error(event.getChannel()));
                } else {
                    API.notAdmin(channel);
                }
            } else {
                API.notGuild(channel);
            }
        };
    }
}