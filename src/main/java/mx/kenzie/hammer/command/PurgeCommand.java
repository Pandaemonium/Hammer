package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgInteger;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

import static mx.kenzie.hammer.Hammer.API;

public class PurgeCommand extends Commander<MessageReceivedEvent> {
    @Override
    protected CommandImpl create() {
        return command("purge")
                .arg((event, input) -> {
                    MessageChannel channel = event.getChannel();
                    User user = event.getAuthor();
                    if (!event.isFromGuild()) {
                        API.notGuild(channel);
                        return;
                    }
                    Guild guild = event.getGuild();
                    GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                    if (!data.allowModActions) return;
                    if (!data.isStaff(user)) {
                        API.notStaff(channel);
                        return;
                    }
                    int amount = (int) input[0];
                    User target = (User) input[1];
                    CompletableFuture<List<Message>> future;
                    if (target != null) {
                        AtomicInteger count = new AtomicInteger();
                        future = channel.getIterableHistory().takeWhileAsync(1000, message -> {
                            if (message.getAuthor().getId().equals(target.getId())) count.getAndIncrement();
                            return count.get() < amount;
                        });
                    } else future = channel.getIterableHistory().takeAsync(amount);

                    future.thenAcceptAsync(messages -> {
                        for (Message message : messages) {
                            message.delete().reason("Purging action.").queue();
                        }
                        data.log(Reaction.WARN_1.getAsMention() + " " + amount + " messages were purged in `" + channel.getName() + "` by " + user.getName() + "");
                    });
                }, new ArgInteger(), new ArgUser().setRequired(false));
    }

    @Override
    public CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            MessageChannel channel = event.getChannel();
            User user = event.getAuthor();
            if (!event.isFromGuild()) {
                API.notGuild(channel);
            } else {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) {
                    API.notStaff(channel);
                    return;
                }
                API.sendMessage(channel, user, new MessageBuilder()
                        .setContent("Correct usage:\n" + "`/purge <amount>`\n" + "`/purge <amount> <user>`")
                        .build())
                        .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
            }
        };
    }
}
