package mx.kenzie.hammer.command;

import com.moderocky.mask.api.temporal.Timespan;
import com.moderocky.mask.command.ArgStringFinal;
import com.moderocky.mask.command.ArgTimespan;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.AuditAction;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.PunishmentManager;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;

public class BanCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("ban")
                .arg(ban(), new ArgUser(), new ArgTimespan(), new ArgStringFinal().setRequired(false))
                .arg(ban(), new ArgUser(), new ArgStringFinal().setRequired(false));
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) return;
                API.sendMessage(channel, user, new MessageBuilder("Correct usage:\n`/ban <id> [reason]`\n`/ban <id> <timespan> [reason]`").build()).queue(API.deleteButton(), API.error(event.getChannel()));
            } else {
                API.notGuild(channel);
            }
        };
    }

    private CommandBiAction<MessageReceivedEvent> ban() {
        return (event, input) -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) {
                    API.notStaff(channel);
                    return;
                }
                User target = (User) input[0];
                Timespan timespan = (input.length == 3) ? (Timespan) input[1] : null;
                String message = (input.length == 3) ? (String) input[2] : (String) input[1];
                guild.retrieveMember(user).queue(member -> {
                    PunishmentManager manager = new PunishmentManager(target, member);
                    manager.complete(timespan, message, true, AuditAction.Type.BAN);
                });
            } else {
                API.notGuild(channel);
            }
        };
    }

}
