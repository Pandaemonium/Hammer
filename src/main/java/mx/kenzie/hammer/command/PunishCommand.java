package mx.kenzie.hammer.command;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moderocky.mask.command.ArgStringFinal;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.PunishmentManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.UUID;

public class PunishCommand extends Commander<MessageReceivedEvent> {
    @Override
    protected CommandImpl create() {
        return command("punish", "pgui")
                .arg((event, input) -> {
                    if (!event.isFromGuild()) {
                        Hammer.API.notGuild(event.getChannel());
                        return;
                    }
                    String reason = (String) input[1];
                    User target = (User) input[0];
                    User user = event.getAuthor();
                    Guild guild = event.getGuild();
                    GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                    if (!data.allowModActions) return;
                    if (Hammer.API.isStaff(user, guild)) {
                        guild.retrieveMember(user).queue(member -> {
                            event.getMessage().delete().reason("Handled.").queue();
                            PunishmentManager manager = new PunishmentManager(target, member);
                            manager.reason = reason;
                            manager.create(url -> user.openPrivateChannel()
                                    .queue(channel -> channel.sendMessage("**Punishment Manager:** `" + target.getName() + "#" + target.getDiscriminator() + "`\n<" + url + ">")
                                            .queue(Hammer.API.deleteButton(), Hammer.API.error(event.getChannel())), failure -> Hammer.API.sendMessage(event.getChannel(), user, "I am unable to private-message you.")));
                        });
                    } else {
                        Hammer.API.notStaff(event.getChannel());
                    }
                }, new ArgUser(), new ArgStringFinal().setRequired(false))
                .arg((event, input) -> {
                    MessageChannel channel = event.getChannel();
                    User user = event.getAuthor();
                    String string = input[0].toString().trim();
                    try {
                        JsonObject object = JsonParser.parseString(string.substring(3, string.length() - 3)).getAsJsonObject();
                        UUID uuid = UUID.fromString(object.get("uuid").getAsString());
                        PunishmentManager manager = PunishmentManager.MANAGERS.get(uuid);
                        if (!manager.guildData.allowModActions) return;
                        if (manager == null) throw new IllegalArgumentException();
                        if (manager.actor.getId().equalsIgnoreCase(user.getId())) {
                            manager.complete(object);
                            Hammer.API.sendMessage(channel, user, "Punishment logged.")
                                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                        }
                    } catch (Throwable throwable) {
                        Hammer.API.sendMessage(channel, user, "Your query could not be formatted!")
                                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                    }
                }, new ArgStringFinal());
    }

    @Override
    public CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            if (event.isFromGuild()) {
                User user = event.getAuthor();
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (Hammer.API.isStaff(user, guild)) {
                    Hammer.API.sendMessage(event.getChannel(), user, "Correct usage: `/punish <user>`")
                            .queue(Hammer.API.deleteButton(), Hammer.API.error(event.getChannel()));
                } else {
                    Hammer.API.notStaff(event.getChannel());
                }
            } else {
                Hammer.API.notGuild(event.getChannel());
            }
        };
    }
}
