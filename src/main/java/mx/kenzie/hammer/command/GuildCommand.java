package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.Commander;
import dev.moderocky.mirror.Mirror;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Invite;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import static mx.kenzie.hammer.Hammer.API;

public class GuildCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("guild")
                .arg("info", event -> {
                            MessageChannel channel = event.getChannel();
                            API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                                    .append(Reaction.NO.getAsMention())
                                    .append(" No guild could be retrieved from your entry.")
                                    .build())
                                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
                        },
                        arg((event, input) -> {
                            Guild guild = (Guild) input[0];
                            User user = event.getAuthor();
                            MessageChannel channel = event.getChannel();
                            getInfo(user, channel, guild);
                        }, new ArgGuild())
                );
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                getInfo(user, channel, guild);
            } else {
                API.notGuild(channel);
            }
        };
    }

    private void getInfo(User user, MessageChannel channel, Guild guild) {
        List<Invite> invites = new MagicList<>();
        try {
            invites = new MagicList<>(guild.retrieveInvites().complete());
        } catch (Throwable ignore) {}
        invites.removeIf(invite -> !invite.isExpanded());
        invites.removeIf(Invite::isTemporary);
        invites.removeIf(invite -> invite.getMaxUses() > 0);
        boolean showInv = Mirror.blank().tryFunc(r -> !Hammer.GUILD_MANAGER.getData(guild).hideInvite, true);
        EmbedBuilder builder = new EmbedBuilder()
                .setColor(new Color(141, 61, 226))
                .setAuthor("Guild Information: " + guild.getName())
                .setThumbnail(guild.getIconUrl())
                .setDescription(guild.getDescription())
                .addField("Total Members", "" + guild.getMemberCount(), true)
                .addField("Total Boosts", "" + guild.getBoostCount(), true)
                .addField("Boost Level", "" + guild.getBoostTier(), true)
                .addField("Guild Owner", "" + guild.retrieveOwner().complete().getEffectiveName(), true)
                .addField("Guild Created", "" + new Date((guild.getIdLong() / 4194304) + 1420070400000L).toGMTString(), true)
                .addField("Guild Invite", "" + (invites.size() > 0 && showInv ? invites.get(0).getUrl() : "None :("), true)
                .addField("Vanity URL", "" + (guild.getVanityCode() != null ? guild.getVanityUrl() : "None :("), true)
                .addField("Verification", "" + guild.getVerificationLevel().name(), true)
                .addField("Region", "" + guild.getRegion().getName() + " (" + guild.getRegion().getEmoji() + ")", true)
                .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                .setTimestamp(Instant.now());
        if (Hammer.API.isAdmin(user, guild)) {
            Guild.MetaData data = guild.retrieveMetaData().complete();
            builder
                    .addField("Member Limit", guild.getMemberCount() + "/" + data.getMemberLimit(), true)
                    .addField("Presence Limit", data.getApproximatePresences() + "/" + data.getPresenceLimit(), true)
                    .addField("Special Features", "" + String.join(", ", guild.getFeatures()), true);
        }
        API.sendMessage(channel, user, new MessageBuilder()
                .setEmbed(builder.build())
                .build())
                .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
    }

}
