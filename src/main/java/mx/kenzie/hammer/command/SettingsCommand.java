package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static mx.kenzie.hammer.Hammer.API;

public class SettingsCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("settings")
                .arg("edit", getDefault(),
                        arg((event, input) -> {
                                    User user = event.getAuthor();
                                    if (!Hammer.API.isBotAdmin(user)) return;
                                    Guild guild = (Guild) input[0];
                                    GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                                    MessageChannel channel = event.getChannel();
                                    API.sendMessage(channel, user, help(guild))
                                            .queue(API.deleteButton(), API.error(channel));
                                    channel.sendFile(data.getConfig(), "settings.json")
                                            .queue(API.deleteButton(), API.error(channel));
                                },
                                new ArgGuild())
                )
                .arg("save", event -> {
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    if (event.isFromGuild()) {
                        Message message = event.getMessage();
                        Guild guild = event.getGuild();
                        GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                        if (!data.isAdmin(user)) {
                            API.notAdmin(channel);
                        } else {
                            saveConfig(message, channel, user, guild, data);
                        }
                    } else {
                        API.notGuild(channel);
                    }
                }, arg(
                        (event, input) -> {
                            User user = event.getAuthor();
                            if (!Hammer.API.isBotAdmin(user)) return;
                            Message message = event.getMessage();
                            MessageChannel channel = event.getChannel();
                            Guild guild = (Guild) input[0];
                            GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                            saveConfig(message, channel, user, guild, data);
                        },
                        new ArgGuild()
                ));
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (!event.isFromGuild()) {
                API.notGuild(channel);
                return;
            }
            Guild guild = event.getGuild();
            GuildData data = Hammer.GUILD_MANAGER.getData(guild);
            if (!API.isAdmin(user, guild)) {
                API.notAdmin(channel);
                return;
            }
            data.editSettings(url -> API.sendMessage(channel, user, help(guild, url))
                    .queue(API.deleteButton(), API.error(channel)));
        };
    }

    private Message help(Guild guild, String url) {
        return new MessageBuilder()
                .setEmbed(new EmbedBuilder()
                        .setColor(new Color(141, 61, 226))
                        .setAuthor("Editing Guild Settings")
                        .setDescription("Your online guild editor can be found [here](" + url + ").\nWhen finished, follow the instructions below to save the data.")
                        .setThumbnail(guild.getIconUrl())
                        .addField("Saving Instructions", Hammer.HAMMER.getResource("saving_instructions.md"), true)
                        .build())
                .build();
    }

    private Message help(Guild guild) {
        return new MessageBuilder()
                .setEmbed(new EmbedBuilder()
                        .setColor(new Color(141, 61, 226))
                        .setAuthor("Editing Guild Settings")
                        .setThumbnail(guild.getIconUrl())
                        .addField("Editing Instructions", Hammer.HAMMER.getResource("editing_instructions.md"), true)
                        .addField("Saving Instructions", Hammer.HAMMER.getResource("saving_instructions.md"), true)
                        .addField("Valid Options", Hammer.HAMMER.getResource("valid_options.md"), false)
                        .build())
                .build();
    }

    private void saveConfig(Message message, MessageChannel channel, User user, Guild guild, GuildData data) {
        if (message.getAttachments().isEmpty()) {
            API.sendMessage(channel, user, help(guild))
                    .queue(API.deleteButton(), API.error(channel));
            return;
        }
        Message.Attachment attachment = message.getAttachments().get(0);
        attachment.retrieveInputStream().whenCompleteAsync((inputStream, throwable) -> {
            try {
                String string = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()
                        .collect(Collectors.joining(System.lineSeparator()));
                data.setConfig(string);
                data.save();
                API.sendMessage(channel, user, Reaction.YES.getAsMention() + " Saved your config!")
                        .queue(API.deleteButton(), API.error(channel));
            } catch (Throwable thr) {
                Throwable blob = throwable != null ? throwable : thr;
                if (Hammer.DEBUG) blob.printStackTrace();
                API.sendMessage(channel, user, Reaction.NO.getAsMention() + " An error occurred while saving your config.\n`" + blob.getMessage() + "`")
                        .queue(API.deleteButton(), API.error(channel));
            }
        });
    }

}
