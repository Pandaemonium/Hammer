package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;

import static mx.kenzie.hammer.Hammer.API;
import static net.dv8tion.jda.api.Permission.*;

public class SetupCommand extends Commander<MessageReceivedEvent> {
    @Override
    public @NotNull CommandImpl create() {
        return command("setup");
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            MessageChannel channel = event.getChannel();
            if (!event.isFromGuild()) {
                channel.sendMessage(new MessageBuilder()
                        .append("This command should be used within a guild!\n")
                        .append("It will guide you through the Hammer setup process and check necessary permissions.")
                        .build())
                        .queue(API.deleteButton(), API.error(channel));
            } else {
                Guild guild = event.getGuild();
                User user = event.getAuthor();
                if (!API.isAdmin(user, guild)) {
                    API.notAdmin(channel);
                    return;
                }
                if (!guild.getSelfMember().hasPermission((GuildChannel) channel, MESSAGE_WRITE, MESSAGE_EMBED_LINKS)) {
                    user.openPrivateChannel().queue(priv -> priv.sendMessage(new MessageBuilder()
                            .append("Hammer is missing the send messages/embeds permissions in that server.")
                            .build())
                            .queue(API.deleteButton(), API.error(priv)));
                } else {
                    Member self = guild.getSelfMember();
                    MagicStringList basic = new MagicList<>(VIEW_CHANNEL, MESSAGE_WRITE, MESSAGE_EMBED_LINKS, MESSAGE_ADD_REACTION, MESSAGE_ATTACH_FILES, MESSAGE_HISTORY, MESSAGE_EXT_EMOJI)
                            .collect(permission -> (self.hasPermission(permission) ? Reaction.YES : Reaction.NO).getAsMention() + " " + permission.getName()).asMagicStrings();
                    MagicStringList staff = new MagicList<>(MESSAGE_MANAGE, MANAGE_CHANNEL, MANAGE_ROLES, VIEW_AUDIT_LOGS, KICK_MEMBERS, BAN_MEMBERS, NICKNAME_MANAGE)
                            .collect(permission -> (self.hasPermission(permission) ? Reaction.YES : Reaction.NO).getAsMention() + " " + permission.getName()).asMagicStrings();
                    MagicStringList admin = new MagicList<>(VIEW_GUILD_INSIGHTS, VIEW_AUDIT_LOGS, MANAGE_SERVER)
                            .collect(permission -> (self.hasPermission(permission) ? Reaction.YES : Reaction.NO).getAsMention() + " " + permission.getName()).asMagicStrings();
                    channel.sendMessage(new EmbedBuilder()
                            .setColor(new Color(141, 61, 226))
                            .setAuthor("Setting up Hammer", null, guild.getSelfMember().getUser().getAvatarUrl())
                            .setDescription(Hammer.HAMMER.getResource("setup.md"))
                            .addField("Basic Functions (Necessary)", String.join("\n", basic), true)
                            .addField("Moderation Functions (Optional)", String.join("\n", staff), true)
                            .addField("Data/Stats Functions (Optional)", String.join("\n", admin), true)
                            .addField("What Next?", "Use `/settings` to edit your guild's unique config, and begin setting up channels and actions.", false)
                            .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                            .setTimestamp(Instant.now())
                            .build())
                            .queue(API.deleteButton(), API.error(channel));
                    try { // Likely to fail, integration role will probably be its highest.
                        self.getRoles().forEach(role -> {
                            if (role.isManaged()) role.getManager().setColor(new Color(141, 61, 226))
                                    .reason("Prettifying Self! :)")
                                    .queue(null, null);
                        });
                    } catch (Throwable ignore) {
                    }
                    GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                    data.setup();
                }
            }
        };
    }
}
