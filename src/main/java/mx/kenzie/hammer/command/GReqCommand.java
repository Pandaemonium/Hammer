package mx.kenzie.hammer.command;

import com.moderocky.mask.command.ArgGuild;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.GuildData;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;

import static mx.kenzie.hammer.Hammer.API;

public class GReqCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("greq")
                .arg("guild", getDefault(),
                        arg(
                                (event, input) -> {
                                    User user = event.getAuthor();
                                    if (!Hammer.API.isBotAdmin(user)) return;
                                    Guild target = (Guild) input[0];
                                    API.sendMessage(event.getChannel(), user, "Sending this guild's data via private message.").queue(API.deleteButton(), API.error(event.getChannel()));
                                    user.openPrivateChannel().queue(channel -> data(channel, target), API.error(event.getChannel()));
                                },
                                new ArgGuild())
                );
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                if (API.isAdmin(user, guild)) {
                    API.sendMessage(channel, user, "Sending this guild's data via private message.").queue(API.deleteButton(), API.error(event.getChannel()));
                    user.openPrivateChannel().queue(chn -> data(chn, guild), API.error(channel));
                } else {
                    API.notAdmin(channel);
                }
            } else {
                API.notGuild(channel);
            }
        };
    }

    private void data(MessageChannel channel, Guild guild) {
        GuildData data = Hammer.GUILD_MANAGER.getData(guild);
        data.save();
        channel
                .sendFile(data.getObject().toString().getBytes(StandardCharsets.UTF_8), "guild_data.json")
                .queue(API.deleteButton(), API.error(channel));
    }
}