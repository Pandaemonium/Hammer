package mx.kenzie.hammer.command;

import com.moderocky.mask.Reaction;
import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.audit.ActionType;
import net.dv8tion.jda.api.audit.TargetType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static mx.kenzie.hammer.Hammer.API;

public class LookupCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("lookup", "history")
                .arg((event, input) -> {
                    User target = (User) input[0];
                    User user = event.getAuthor();
                    MessageChannel channel = event.getChannel();
                    if (event.isFromGuild()) {
                        Guild guild = event.getGuild();
                        List<String> audits = new ArrayList<>();
                        List<String> actions = Hammer.USER_MANAGER.getData(target).getHistory(guild);
                        if (API.isStaff(user, guild)) {
                            guild.retrieveAuditLogs().cache(false).forEachRemaining(value -> {
                                if (value.getTargetType() == TargetType.MEMBER && value.getTargetId().equalsIgnoreCase(target.getId())) {
                                    if (value.getType() == ActionType.BAN)
                                        audits.add("Banned by: " + (value.getUser() != null ? value.getUser().getName() + "#" + value.getUser().getDiscriminator() : "Unknown") + " for: " + value.getReason());
                                    else if (value.getType() == ActionType.KICK)
                                        audits.add("Kicked by: " + (value.getUser() != null ? value.getUser().getName() + "#" + value.getUser().getDiscriminator() : "Unknown") + " for: " + value.getReason());
                                    else if (value.getType() == ActionType.MEMBER_VOICE_KICK)
                                        audits.add("VC-Removed by: " + (value.getUser() != null ? value.getUser().getName() + "#" + value.getUser().getDiscriminator() : "Unknown") + " for: " + value.getReason());
                                }
                                return (audits.size() < 10);
                            });
                            API.sendMessage(channel, user, new EmbedBuilder()
                                    .setColor(new Color(141, 61, 226))
                                    .setAuthor("Punishment History: " + target.getName() + "#" + target.getDiscriminator(), null, target.getAvatarUrl())
                                    .setDescription(Hammer.HAMMER.getResource("history.md"))
                                    .addField("Hammer Audit Logs", actions.size() > 0 ? String.join("\n", actions) : "None!", true)
                                    .addField("Discord Audit Logs", audits.size() > 0 ? String.join("\n", audits) : "None!", true)
                                    .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                                    .setTimestamp(Instant.now())
                                    .build())
                                    .queue(API.deleteButton(), API.error(event.getChannel()));
                        } else {
                            API.notStaff(channel);
                        }
                    } else {
                        API.notGuild(channel);
                    }
                }, new ArgUser());
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            MessageChannel channel = event.getChannel();
            API.sendMessage(channel, event.getAuthor(), new MessageBuilder()
                    .append(Reaction.NO.getAsMention())
                    .append(" No user could be retrieved from your entry.\n")
                    .append("`/lookup <user-id>`")
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        };
    }
}