package mx.kenzie.hammer.command;

import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.time.Instant;

import static mx.kenzie.hammer.Hammer.API;

public class InfoCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("info");
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            JDA jda = event.getJDA();
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            long members = 0;
            int pub = 0;
            int part = 0;
            int veri = 0;
            String invite = jda.getInviteUrl(Permission.ADMINISTRATOR);
            for (Guild guild : jda.getGuilds()) {
                members += guild.getMemberCount();
                if (guild.getFeatures().contains("PUBLIC")) pub++;
                if (guild.getFeatures().contains("PARTNERED")) part++;
                if (guild.getFeatures().contains("VERIFIED")) veri++;
            }
            API.sendMessage(channel, user, new MessageBuilder()
                    .setEmbed(new EmbedBuilder()
                            .setColor(new Color(141, 61, 226))
                            .setAuthor("Info for Hammer " + (jda.getShardManager() != null ? "(Shard)" : "(Complete)"))
                            .setThumbnail(jda.getSelfUser().getAvatarUrl())
                            .setDescription(Hammer.HAMMER.getResource("description.md") + "\n[Need a Hammer?](" + invite + ")")
                            .addField("Handled Guilds", "" + jda.getGuilds().size(), true)
                            .addField("Handled Users", "" + members, true)
                            .addField("Total Shards", "" + (jda.getShardManager() != null ? jda.getShardManager().getShardsTotal() : 1), true)
                            .addField("Public Servers", "" + pub, true)
                            .addField("Partnered Servers", "" + part, true)
                            .addField("Verified Servers", "" + veri, true)
                            .setFooter("Queried: " + user.getName() + "#" + user.getDiscriminator())
                            .setTimestamp(Instant.now())
                            .build())
                    .build())
                    .queue(Hammer.API.deleteButton(), Hammer.API.error(channel));
        };
    }

}
