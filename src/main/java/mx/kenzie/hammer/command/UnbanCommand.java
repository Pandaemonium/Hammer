package mx.kenzie.hammer.command;

import com.moderocky.mask.command.ArgUser;
import com.moderocky.mask.command.Commander;
import mx.kenzie.hammer.Hammer;
import mx.kenzie.hammer.data.AuditAction;
import mx.kenzie.hammer.data.GuildData;
import mx.kenzie.hammer.data.PunishmentManager;
import mx.kenzie.hammer.data.UserData;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import static mx.kenzie.hammer.Hammer.API;
import static mx.kenzie.hammer.Hammer.USER_MANAGER;

public class UnbanCommand extends Commander<MessageReceivedEvent> {

    @Override
    public @NotNull CommandImpl create() {
        return command("unban")
                .arg(unban(), new ArgUser());
    }

    @Override
    public @NotNull CommandSingleAction<MessageReceivedEvent> getDefault() {
        return event -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) return;
                API.sendMessage(channel, user, new MessageBuilder("Correct usage: `/unban <id>`").build()).queue(API.deleteButton(), API.error(event.getChannel()));
            } else {
                API.notGuild(channel);
            }
        };
    }

    private CommandBiAction<MessageReceivedEvent> unban() {
        return (event, input) -> {
            User user = event.getAuthor();
            MessageChannel channel = event.getChannel();
            if (event.isFromGuild()) {
                Guild guild = event.getGuild();
                GuildData data = Hammer.GUILD_MANAGER.getData(guild);
                if (!data.allowModActions) return;
                if (!data.isStaff(user)) {
                    API.notStaff(channel);
                    return;
                }
                User target = (User) input[0];
                UserData userData = USER_MANAGER.getData(target);
                guild.unban(target).queue();
                userData.setActions(userData.getActions().collect(action -> {
                    if (action.type == AuditAction.Type.BAN && !action.isExpired()) {
                        action.expired = true;
                        PunishmentManager.logRevokeAction(user, action);
                    }
                    return action;
                }));
                userData.addAction(PunishmentManager.createUnbanAction(user.getIdLong(), target.getIdLong(), guild.getIdLong()));
                userData.save();
                userData.updateAPI();
                API.sendMessage(channel, user, "User has been un-banned.").queue(API.deleteButton(), API.error(channel));
            } else {
                API.notGuild(channel);
            }
        };
    }

}
