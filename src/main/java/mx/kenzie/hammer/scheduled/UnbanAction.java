package mx.kenzie.hammer.scheduled;

import com.google.gson.JsonObject;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;

import java.time.Instant;

public class UnbanAction extends GuildUserAction {

    public UnbanAction(Instant time, User user, Guild guild) {
        super(time, user, guild);
    }

    public UnbanAction(JsonObject object) {
        super(object);
    }

    @Override
    public void run() {
        if (complete) return;
        complete = true;
        Guild guild = Hammer.getJda().getGuildById(this.guild);
        if (guild != null)
            Hammer.getJda().retrieveUserById(user).queue(u -> guild.unban(u).reason("Expiry").queue());
    }
}
