package mx.kenzie.hammer.scheduled;

import com.google.gson.JsonObject;
import com.moderocky.mask.api.temporal.TemporalAction;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;

import java.time.Instant;

public abstract class GuildUserAction extends TemporalAction {
    public final long user;
    public final long guild;

    public GuildUserAction(Instant time, User user, Guild guild) {
        super(time);
        this.user = user.getIdLong();
        this.guild = guild.getIdLong();
    }

    public GuildUserAction(JsonObject object) {
        super(object);
        this.user = object.get("user").getAsLong();
        this.guild = object.get("guild").getAsLong();
    }

    @Override
    public JsonObject getAsJson() {
        JsonObject object = new JsonObject();
        object.addProperty("user", user);
        object.addProperty("guild", guild);
        return object;
    }
}
