package mx.kenzie.hammer.scheduled;

import com.google.gson.JsonObject;
import mx.kenzie.hammer.Hammer;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;

import java.time.Instant;

public class UnmuteAction extends GuildUserAction {

    public UnmuteAction(Instant time, User user, Guild guild) {
        super(time, user, guild);
    }

    public UnmuteAction(JsonObject object) {
        super(object);
    }

    @Override
    public void run() {
        if (complete) return;
        complete = true;
        Guild guild = Hammer.getJda().getGuildById(this.guild);
        if (guild != null)
            Hammer.getJda().retrieveUserById(user).queue(u -> Hammer.GUILD_MANAGER.getData(guild).unmute(u, null));
    }
}
