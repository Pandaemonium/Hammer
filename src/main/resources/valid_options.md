`guild_rules` - A multi-line string of rules for your server.
`mute_role` - The 'muted' role ID the bot should assign.
`staff_role` - Users with this role ID can use staff commands.
`log_channel` - The channel ID where the bot should log actions.
`respond_via_dm` - If the bot should respond to users via DM.
`allow_auto_mutes` - If the bot should attempt to prevent spam.
`hide_guild_invite` - Should the bot hide your invite in `/guild`?