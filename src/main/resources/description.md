Hammer is a Discord management and moderation system created by [Mackenzie](https://kenzie.mx) in order to handle large discords requiring complex management.
The system is designed to fill the role of a moderator as well as providing other useful tools and global integrations.

For support, please contact the creator, ([Mackenbee#0001](https://discord.gg/G6ca9S5)).