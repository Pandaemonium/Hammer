```vb
/help # Displays this. :)
/info # View general Hammer stats.
/guild [info <guild>] # Display the in-depth information of a guild.
/user  [info <user>]  # Display the in-depth information of a user.
/req # Requests data cached about your user (punishments, etc.)
     # This will come as a (non-editable) file!
```