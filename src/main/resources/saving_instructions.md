To save the config, use the command `/settings save` and attach the finished file to your message.
The file must be correctly-parsed JSON (though the layout doesn't matter.)
If there is an error in your file, it will not be saved and you will be notified.