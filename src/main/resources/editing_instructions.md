The local config for this guild is attached.
You may edit any field, keeping with a correct json format.
To reset a field to default/null, simply delete the entry, or set it to `null`.
You may reset the entire config using `/settings reset`.