```vb
/lookup <user> # Display punishments assigned to a user from this guild.
/warn   <user> [reason...] # Warns a user with the given reason.
/mute   <user> [reason...] # Mutes a user with the given reason.
/kick   <user> [reason...] # Kicks a user with the given reason.
/ban    <user> [reason...] # Bans a user with the given reason.
/unmute <user> # Unmutes a user with the given reason.
/unban  <user> # Unbans a user with the given reason.
/purge  <int>  # Removes N messages (+ the command.)
/req    <user> # Requests the data of a user (guild punishments, etc.)
               # This will come as a (non-editable) file!
```