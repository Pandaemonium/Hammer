Below are listed most of Hammer's basic commands.
If you need more detailed help, please consult the wiki [here](https://gitlab.com/Pandaemonium/Hammer).

This only displays relevant commands you can access.