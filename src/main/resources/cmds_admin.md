```vb
/settings # Display/edit your guild's settings.
          # This is an editable config!
          # Attach the finished file to save.
/greq # Displays your stored guild data.
      # This is a (non-editable) file!
/gclear # Resets your custom guild data/settings to factory.
        # This may remove punishment history from users.
```