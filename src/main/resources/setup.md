Most of Hammer's setup is carried out automatically by the bot, as long as it has the necessary permissions.
It is recommended to give the bot an administrator role and place this role above normal user roles, but below staff roles. If you are concerned about security, remember that the bot is entirely [open-source](https://gitlab.com/Pandaemonium/Hammer) and that the token is regenerated regularly to prevent misuse.

In case you are still concerned, below are listed the required permissions for each function. These are necessary permissions for the action to work - lacking them will result in a soft failure.

